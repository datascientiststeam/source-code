/*
 * Copyright 2012 Sentric. All rights reserved.
 * https://github.com/sentric/storm-example
 */

package main.java.ch.sentric.storm.example.topology;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.utils.Utils;
import main.java.ch.sentric.storm.example.bolt.ClientExtractorBolt;
import main.java.ch.sentric.storm.example.bolt.RedisIncrementBolt;
import main.java.ch.sentric.storm.example.spout.TwitterSampleSpout;


public class TwitterClientCountTopology {
	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		final TopologyBuilder builder = new TopologyBuilder();
        final String username = "ishwarpohani";
        final String pwd = "jai123khori";

		final String redisHost = "127.0.0.1";
		final int redisPort = 6379;
		final int redisDb = 2;

		builder.setSpout("sample", new TwitterSampleSpout(username, pwd));
		builder.setBolt("client", new ClientExtractorBolt()).shuffleGrouping("sample");
		//builder.setBolt("redis", new RedisIncrementBolt("clients", redisHost, redisPort, redisDb)).shuffleGrouping("client");
		final Config conf = new Config();

		final LocalCluster cluster = new LocalCluster();

		cluster.submitTopology("test", conf, builder.createTopology());

		Utils.sleep(5 * 60 * 1000);
		cluster.shutdown();
	}

}
