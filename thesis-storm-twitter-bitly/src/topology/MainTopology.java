package topology;

import java.util.Date;

import common.DataStreamEmitterBolt;
import common.ShowCosineSimilarityBolt;
import common.TFIDFAndConsineSimilarityCalculationBolt;

import twitter.bolts.TwitterTittleTImeWindowBolt;
import twitter.bolts.ClientExtractorBolt2;
import twitter.spouts.TwitterFilterSpout;
import twitter.spouts.TwitterSampleSpout;
import utility.Configuration;
import utility.GlobalTime;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.utils.Utils;
import bitly.bolts.BitlyDataPrinterBolt;
import bitly.bolts.BitlyTittleTimeWindowBolt;
import bitly.spouts.BiltyDataStreamSpout;
import bitly.spouts.BiltyHotPhrasesSpout;

public class MainTopology {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final TopologyBuilder builder = new TopologyBuilder();
        final String username = "ishwarpohani";
        final String pwd = "jai123khori";

        builder.setSpout("twittersample", new TwitterFilterSpout(username, pwd,Configuration.DEBUG_TWITTER_STREAM));
		builder.setBolt("twitterTitleWindowStream", new TwitterTittleTImeWindowBolt()).shuffleGrouping("twittersample");
		//builder.setBolt("printTwitterStream", new ClientExtractorBolt2()).shuffleGrouping("twitterTitleWindowStream");
		
        builder.setSpout("bitlyStream", new BiltyDataStreamSpout()); 
		builder.setBolt("bitlyTitleWindowStream", new BitlyTittleTimeWindowBolt()).shuffleGrouping("bitlyStream");
		//builder.setBolt("printerBitlyStream", new BitlyDataPrinterBolt()).shuffleGrouping("bitlyTitleWindowStream");
		
		//Common Bolt
		builder.setBolt("dataStreamWindow", new DataStreamEmitterBolt()).shuffleGrouping("twitterTitleWindowStream").shuffleGrouping("bitlyTitleWindowStream");
		
		int totleBoltsForTFIDF = 2;
		builder.setBolt("consineSimilarity1", new TFIDFAndConsineSimilarityCalculationBolt(0,totleBoltsForTFIDF)).shuffleGrouping("dataStreamWindow");
		builder.setBolt("consineSimilarity2", new TFIDFAndConsineSimilarityCalculationBolt(1,totleBoltsForTFIDF)).shuffleGrouping("dataStreamWindow");
		builder.setBolt("results", new ShowCosineSimilarityBolt()).shuffleGrouping("consineSimilarity1").shuffleGrouping("consineSimilarity2");

		
		final Config conf = new Config();

		final LocalCluster cluster = new LocalCluster();
		
		//Start Timmer Before start topology
		GlobalTime timer = new GlobalTime(new Date());
        Thread t = new Thread(timer);
        t.start();
        
		cluster.submitTopology("test", conf, builder.createTopology());

		Utils.sleep(5 * 60 * 1000);
		cluster.shutdown();
	}

}
