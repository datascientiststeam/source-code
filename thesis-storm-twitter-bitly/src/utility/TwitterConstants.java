package utility;

/**
 * @author Ishwar
 * 
 *
 */
public class TwitterConstants {

	/**
	 * 
	 * @return
	 */
	public static double[][] getPakistanLocationBoundryBox(){

        // http://stackoverflow.com/questions/16512878/passing-longitude-and-latitude-in-twitter-streaming-api-of-pakistan
        // http://stackoverflow.com/questions/14800081/is-it-possible-to-define-location-as-double
        // https://github.com/vr3690/TwiLoc
		
		/**
        * Upper/northern latitude that marks the
        * upper bounds of the geographical area
        * for which tweets need to be analysed.
        */
        double northLatitude = 35.2;
        /**
        * Lower/southern latitude. Marks the lower bound.
        */
        double southLatitude = 25.2;
        /**
        * Eastern/left longitude. Marks the left-side bounds.
        */
        double eastLongitude = 73.3;
        /**
        * Western/right longitude. Marks the right-side bounds.
        */
        double westLongitude = 62.9;
        
        double pakistanLcationBox[][] = {{westLongitude, southLatitude},{eastLongitude, northLatitude}};
        
        return pakistanLcationBox;
	}
}