package utility;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPURLConnectionUtility {

	public static final String METHOD_POST = "POST";
	public static final String METHOD_GET = "GET";
	private final String USER_AGENT = "Mozilla/5.0";
	
	
	private String url;
	private String method;
	private boolean debug;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	

	public boolean isDebug() {
		return debug;
	}
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	public String getResponseData() throws Exception{
		try{
			if(!Utility.isEmpty(this.getUrl())){
				if(this.getMethod().equals(METHOD_POST)){
					return this.sendPost();
				}else{
					return this.sendGet();
				}
			}
			
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
		return "";
	}
	
	// HTTP GET request
	private String sendGet() throws Exception {
 
		//String url = "https://api-ssl.bitly.com";
		//String parameter = "/v3/highvalue?access_token=e37cd24453ed2be4a306cccc0565c2e3332c4455"; 
		
		URL obj = new URL(this.getUrl());
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		// optional default is GET
		con.setRequestMethod("GET");
 
		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
 
		int responseCode = con.getResponseCode();
		if(this.isDebug()){
			System.out.println("\nSending 'GET' request to URL : " + this.getUrl());
			System.out.println("Response Code : " + responseCode);
		}
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		//System.out.println(response.toString());
		return response.toString();
 
	}
	
	// HTTP POST request
	private String sendPost() throws Exception {
 
		//String url = "https://www.google.com/search?q=mkyong";
		
//		String redirectUri = "http://cgeers.com/";
//		String code = "c7c3569311a8f88db23ec88431eb66fda033aa5b";
//		String clientId = "0baf07bd006289784b8503268acbd9ba82a0894a";
//		String clientSecret = "4af841d1ed5915c8e0528e823ffdc0726f2ee6e2";
//		
//		String requestUri = "https://api-ssl.bitly.com/oauth/access_token";
//		requestUri += "?client_id="+clientId+"&";
//		requestUri += "client_secret="+clientSecret+"&";
//		requestUri += "code="+code+"&";
//		requestUri += "redirect_uri="+redirectUri+"";
		
		
		URL obj = new URL(this.getUrl());
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
 
		//String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes("");
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
		if(this.isDebug()){
			System.out.println("\nSending 'POST' request to URL : " + this.getUrl());
			System.out.println("Post parameters : " + "");
			System.out.println("Response Code : " + responseCode);
		}
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		//System.out.println(response.toString());
		return response.toString();
 
	}
}
