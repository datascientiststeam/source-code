/*
 * Copyright 2012 Sentric. All rights reserved.
 */
package twitter.bolts;

import java.util.Date;

import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dto.DataStream;

import twitter4j.Status;
import utility.GlobalTime;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class ClientExtractorBolt2 extends BaseRichBolt {
	private static final long serialVersionUID = 4150732456907856015L;
	private OutputCollector collector;
	Pattern clientPattern = Pattern.compile("<a[^>]+>([^<]+)</a>");
	
	
	Date startDate = null;
	
	LinkedBlockingQueue<Status> queue = null;

	@Override
	public void prepare(final Map stormConf, final TopologyContext context,
			final OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void execute(final Tuple input) {
		
		
		DataStream twitterDataStream =  (DataStream) input.getValue(0);
		
		final LinkedBlockingQueue<Status> statusQueues = twitterDataStream.getTwitterDataStream();
		System.out.println("-------------------- Start : Twitter Stream  " + GlobalTime.nextTitleTimeWindowDateTime +" ----------------------");
		if(statusQueues!=null && statusQueues.size()>0){
			for(Status q : statusQueues){
				System.out.println("@" + q.getUser().getScreenName() + " - " + q.getText());
			}
		}
		System.out.println("-------------------- End : Twitter Stream ----------------------");

		collector.emit(input, new Values(statusQueues));
		collector.ack(input);
		
		//final String client = status.getSource();
		
		
		//System.out.println("@" + status.getUser().getScreenName() + " - " + status.getText());
		
//		if (client.equals("web")) {
//			collector.emit(input, new Values(client));
//		} else {
//			final Matcher matcher = clientPattern.matcher(client);
//			if (matcher.matches()) {
//				collector.emit(input, new Values(matcher.group(1)));
//			} else {
//				collector.emit(input, new Values("unknown"));
//			}
//		}
		
		
	}
	@Override
	public void declareOutputFields(final OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("client2"));
	}
}