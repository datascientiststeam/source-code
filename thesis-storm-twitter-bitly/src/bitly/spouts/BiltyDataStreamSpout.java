/*
 * Copyright 2012 Sentric. All rights reserved.
 */
package bitly.spouts;

import backtype.storm.Config;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import bitlyData.BurstingPhrases;
import bitlyData.FilterPhrases;
import bitlyData.HotPhrases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import utility.Configuration;

import org.json.JSONArray;
import org.json.JSONObject;

import dto.BitlyDataDTO;

public class BiltyDataStreamSpout extends BaseRichSpout {
	private static final long serialVersionUID = -2871793574597747583L;
	SpoutOutputCollector _collector;
    LinkedBlockingQueue<BitlyDataDTO> queue = null;//List of phrases
    TwitterStream _twitterStream;
    
    boolean stopDataStream = true;
    
    
    public BiltyDataStreamSpout() {
    }
    
    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        queue = new LinkedBlockingQueue<BitlyDataDTO>();
        _collector = collector;
        try{
        	
			FilterPhrases filterPhrases = new Configuration().preparetBitlyFilterPhrasesCriteria();
			ArrayList<BitlyDataDTO> bitlyDataList = filterPhrases.getFilterPhrases();
			if(bitlyDataList!=null && bitlyDataList.size()>0){
				for(BitlyDataDTO bitlyData : bitlyDataList){
					queue.offer(bitlyData);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        
    }
    
    public void addMoreDataInQueue() {
        queue = new LinkedBlockingQueue<BitlyDataDTO>();
        try{
        	FilterPhrases filterPhrases = new Configuration().preparetBitlyFilterPhrasesCriteria();
			ArrayList<BitlyDataDTO> bitlyDataList = filterPhrases.getFilterPhrases();
			if(bitlyDataList!=null && bitlyDataList.size()>0){
				for(BitlyDataDTO bitlyData : bitlyDataList){
					queue.offer(bitlyData);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    @Override
    public void nextTuple() {
    	BitlyDataDTO ret = queue.poll();
        if(ret==null) {
        	Utils.sleep(5*1000);
        	addMoreDataInQueue();
            
        } else {
            _collector.emit(new Values(ret));
        }
    }

    @Override
    public void close() {
    	stopDataStream = false;
        //_twitterStream.shutdown();
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config ret = new Config();
        ret.setMaxTaskParallelism(1);
        return ret;
    }    

    @Override
    public void ack(Object id) {
    }

    @Override
    public void fail(Object id) {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweet"));
    }
    
}
