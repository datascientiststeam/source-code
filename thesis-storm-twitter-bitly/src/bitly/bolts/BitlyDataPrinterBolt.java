/*
 * Copyright 2012 Sentric. All rights reserved.
 */
package bitly.bolts;

import java.util.Date;
import java.util.HashMap;

import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dto.BitlyDataDTO;
import dto.DataStream;

import twitter4j.Status;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class BitlyDataPrinterBolt extends BaseRichBolt {
	private static final long serialVersionUID = 4150732456907856015L;
	private OutputCollector collector;
	Pattern clientPattern = Pattern.compile("<a[^>]+>([^<]+)</a>");
	
	
	@Override
	public void prepare(final Map stormConf, final TopologyContext context,
			final OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void execute(final Tuple input) {
		
		DataStream bitlyDataStream =  (DataStream) input.getValue(0);
		
		final HashMap<String,BitlyDataDTO> statusQueues = bitlyDataStream.getBitlyDataStream();
		
		System.out.println("-------------------- Start : Bitly Stream ----------------------");
		
		if(statusQueues!=null && statusQueues.size()>0){
			for(BitlyDataDTO q : statusQueues.values()){
				System.out.println("@" + " - " + q.getTitle() + " - " + q.getLang() + " - "+ q.getCities());
			}
		}
		
		System.out.println("-------------------- End : Bitly Stream ----------------------");
		
		collector.emit(input, new Values(statusQueues));
		collector.ack(input);
		
	}
	@Override
	public void declareOutputFields(final OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("client2"));
	}
}