/*
 * Copyright 2012 Sentric. All rights reserved.
 */
package bitly.bolts;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import twitter4j.Status;
import utility.Configuration;
import utility.GlobalTime;
import utility.IConstants;
import utility.Utility;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import org.json.JSONObject;

import dto.BitlyDataDTO;
import dto.DataStream;

public class BitlyTittleTimeWindowBolt extends BaseRichBolt {
	private static final long serialVersionUID = 4150732456907856015L;
	private OutputCollector collector;
	Pattern clientPattern = Pattern.compile("<a[^>]+>([^<]+)</a>");
	
	Date startDate = null;
	
	//HashMap<String,BitlyDataDTO> queue = null;
	HashMap<Date,HashMap<String,BitlyDataDTO>> queueByTime = new HashMap<Date,HashMap<String,BitlyDataDTO>>();


	@Override
	public void prepare(final Map stormConf, final TopologyContext context,
			final OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void execute(final Tuple input) {
		try{
			final BitlyDataDTO bitlyData = (BitlyDataDTO) input.getValue(0);
			

			Date currentDate = new Date();

			//Get url
			String link = bitlyData.getAggregate_link();
			if(Utility.isEmpty(link)){
				bitlyData.getUrl();
			}

			//Current title time window datetime
			Date currentTitleTimeWindow = GlobalTime.nextTitleTimeWindowDateTime;
			
			//Prepare Bitly data by title time window datetime
			HashMap<String,BitlyDataDTO> queue= queueByTime.get(currentTitleTimeWindow);
			if(queue==null){
				queue = new HashMap<String,BitlyDataDTO>();
				queueByTime.put(currentTitleTimeWindow, queue);
			}
			queue.put(link, bitlyData);
			
			//Emit data stream to other bolt
			if(queueByTime!=null && queueByTime.size()>0){
				
				ArrayList<Date> removeKeys = new ArrayList<Date>();
				
				for (Entry<Date, HashMap<String, BitlyDataDTO>> queueTime : queueByTime.entrySet()) {
					
				    //System.out.println("Key = " + queueTime.getKey() + ", Value = " + queueTime.getValue());
					Date time = queueTime.getKey();
					HashMap<String, BitlyDataDTO> currentQueue = queueTime.getValue();
					
					//Check time against global clock
					if(currentDate.equals(time) || currentDate.after(time)){
						
						//Needs to remove this queue against time
						removeKeys.add(time);
						
						//Debug on then show tweets arrival time
						if(Configuration.DEBUG_TITLE_TIME_WINDOW_TIMEDATE){
							System.out.println("------------------Title Time Window Current (Bitly) :  " + time);
						}
						
						//Preapare Data Stream
						DataStream bitlyDataStream = new DataStream();
						bitlyDataStream.setDataStreamCategory(IConstants.BITLY_DATA_STREAM);
						bitlyDataStream.setBitlyDataStream(currentQueue);
						bitlyDataStream.setBitlyTitleTimeWindowTime(time);
						bitlyDataStream.setTitleTimeWindowTime(time);
						
						//Send data to other bolt
						collector.emit(input, new Values(bitlyDataStream));
						collector.ack(input);
						
					}
				}
				
				//Remove values which are emit to other bolts
				if(removeKeys!=null && removeKeys.size()>0){
					for(Date key:removeKeys){
						queueByTime.remove(key);
					}
				}
			}
		}catch(Exception e){
			System.out.println(e);
		}
	}

	@Override
	public void declareOutputFields(final OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("client"));
	}

}
