package NLP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import dto.DataStream;

public class CosineSimilarity {

	private DocumentsTFIDF DocumentsTFIDF;
	
	public CosineSimilarity(DocumentsTFIDF tfIdf){
		this.DocumentsTFIDF = tfIdf;
	}
	
	public Double similarityBetweenTwoDocuments(Document doc1, Document doc2){
		return similarityBetweenTwoDocuments(doc1.getDocumentId(),doc2.getDocumentId());
	}
	
	public Double similarityBetweenTwoDocuments(int documentId1, int documentId2){
		
		Double cosineSimilarity = 0.0;
		
		if(DocumentsTFIDF!=null){
			
			
			ArrayList<Integer> documentIds = new ArrayList<Integer>();
			documentIds.add(documentId1);
			documentIds.add(documentId2);
			HashMap<Integer, HashMap<String, Double>> tfIdfByDocIds = DocumentsTFIDF.getTFIDFByDocumentIds(documentIds);
			
			if(tfIdfByDocIds!=null && tfIdfByDocIds.size()>0){
				
				HashMap<String, Double> TFIDFDoc1 = new HashMap<String, Double>(); 
				HashMap<String, Double> TFIDFDoc2 = new HashMap<String, Double>(); 
				
				for (Entry<Integer, HashMap<String, Double>> tfIdfByWord : tfIdfByDocIds.entrySet()) {
					Integer docId  = tfIdfByWord.getKey();
					
					if(docId==documentId1){
						TFIDFDoc1 = tfIdfByWord.getValue();
					}
					if(docId==documentId2){
						TFIDFDoc2 = tfIdfByWord.getValue();
					}
				}
				
				/************************************************************************************************/
				/**                                TF-IDF - OF XYZ TABLE                                       **/
				/************************************************************************************************/
				/****        ***     glutathione   ***    homocystine    ***   Coa  ***   Transhydrogenase   ****/
				/************************************************************************************************/
				/****  Doc1  ***          1        ***        1          ***    0   ***         1            ****/
				/************************************************************************************************/
				/****  Doc2  ***          2        ***        0          ***    1   ***         1            ****/
				/************************************************************************************************/
				/**                                          Cosine similarity                                 **/
				/**                                                                                            **/
				/** cos(doc1,doc2) = (doc1.doc2) / (sqrt(sum of square(doc1) + sqrt(sum of square(doc1))       **/
				/**                                                                                            **/
				/**  ((1*2) + (1*0) + (0*1) + (1*1)                                                            **/
				/**  ---------------------------------------------------------------------------               **/
				/**  (sqrt((1)^2 + (1)^2 + (0)^2 + (1)^2)) * (sqrt((2)^2 + (0)^2 + (1)^2 + (1)^2)              **/
				/**                                                                                            **/
				/************************************************************************************************/
				
				
				
				Double numerator = 0.0;
				Double denminator = 0.0;

				//Calculate Numerator
				if(TFIDFDoc1!=null && TFIDFDoc1.size()>0 && TFIDFDoc2!=null && TFIDFDoc2.size()>0){
					Set<String> words = TFIDFDoc1.keySet();
					if(words!=null && words.size()>0){
						for(String word: words){
							Double tfidfDoc1 = TFIDFDoc1.get(word);
							Double tfidfDoc2 = TFIDFDoc2.get(word);
							numerator += (tfidfDoc1 * tfidfDoc2);
						}
					}
				}
				
				//Calculate Denomenator
				Double denminator1 = 0.0; //square root (Under (square power 2 to each tfidf))
				if(TFIDFDoc1!=null && TFIDFDoc1.size()>0){
					Set<String> words = TFIDFDoc1.keySet();
					if(words!=null && words.size()>0){
						for(String word: words){
							Double tfidfDoc1 = TFIDFDoc1.get(word);
							denminator1 += (tfidfDoc1 * tfidfDoc1);
						}
						denminator1 = Math.sqrt(denminator1);
					}
				}
				Double denminator2 = 0.0; //square root (Under (square power 2 to each tfidf))
				if(TFIDFDoc2!=null && TFIDFDoc2.size()>0){
					Set<String> words = TFIDFDoc2.keySet();
					if(words!=null && words.size()>0){
						for(String word: words){
							Double tfidfDoc2 = TFIDFDoc2.get(word);
							denminator2 += (tfidfDoc2 * tfidfDoc2);
						}
						denminator2 = Math.sqrt(denminator2);
					}
				}
				denminator = denminator1 * denminator2;
				
				
				if(denminator>0){
					cosineSimilarity = numerator / denminator;
				}
				
			}
			
		}
		return cosineSimilarity;
	}
	
	 
	
	
}
