package NLP;

public class DocumentManager {
	
	private static int documentId = 1;
	
	public Document createDoument(String documentContent){
		
		Document doc = new Document(documentContent , documentId);
		documentId++;
		
		return doc;
	}

}
