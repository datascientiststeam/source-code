package bitlyData;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import dto.BitlyDataDTO;

import utility.BitlyKeys;
import utility.HTTPURLConnectionUtility;
import utility.Utility;



public class FilterPhrases {
	
	private String query; //string to query for.
	private String cities; //show links active in this city (ordered like country-state-city, e.g. us-il-chicago).
	private String limit; // the maximum number of links to return.
	private String offset; //which result to start with (defaults to 0).
	private String lang; //favor results in this language (two letter ISO code).
	private String domain; // restrict results to this web domain (like bitly.com).
	private String full_domain; // restrict results to this full web domain (like blog.bitly.com).
	private String fields; // which fields to return in the response (comma-separated). May be any of: domain, initial_epoch, h2, h3, site, lastindexed, keywords, last_indexed_epoch, title, initial, summaryText, content, score, summaryTitle, type, description, cities, lang, url, referrer, aggregate_link, lastseen, page, ogtitle, aggregate_link. By default, all will be returned.
	private boolean debug;
	
	public String getQuery() {
		return query;
	}

	/**
	 * string to query for.
	 * @param query
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	public String getCities() {
		return cities;
	}

	/**
	 * show links active in this city (ordered like country-state-city, e.g. us-il-chicago).
	 * @param cities
	 */
	public void setCities(String cities) {
		this.cities = cities;
	}

	public String getLimit() {
		return limit;
	}

	/**
	 * the maximum number of links to return.
	 * @param limit
	 */
	public void setLimit(String limit) {
		this.limit = limit;
	}

	public String getOffset() {
		return offset;
	}

	/**
	 * which result to start with (defaults to 0).
	 * @param offset
	 */
	public void setOffset(String offset) {
		this.offset = offset;
	}

	public String getLang() {
		return lang;
	}

	/**
	 * favor results in this language (two letter ISO code). (E.g: For English "en")
	 * 
	 * @param lang
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getDomain() {
		return domain;
	}

	/**
	 * restrict results to this web domain (like bitly.com).
	 * @param domain
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getFull_domain() {
		return full_domain;
	}

	/**
	 * restrict results to this full web domain (like blog.bitly.com).
	 * @param full_domain
	 */
	public void setFull_domain(String full_domain) {
		this.full_domain = full_domain;
	}

	public String getFields() {
		return fields;
	}

	/**
	 * which fields to return in the response (comma-separated). May be any of: domain, initial_epoch, h2, h3, site, lastindexed, keywords, last_indexed_epoch, title, initial, summaryText, content, score, summaryTitle, type, description, cities, lang, url, referrer, aggregate_link, lastseen, page, ogtitle, aggregate_link. By default, all will be returned.
	 * @param fields
	 */
	public void setFields(String fields) {
		this.fields = fields;
	}
	

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public ArrayList<BitlyDataDTO> getFilterPhrases(){
		
		//https://api-ssl.bitly.com/v3/search?query=%20&cities=pk&lang=en&format=json&fields=lang%2Ccities%2Caggregate_link%2Ctitle%2Curl%2CsummaryText
		
		ArrayList<BitlyDataDTO> bitlyDataList = new ArrayList<BitlyDataDTO>();
		
		try{
		String url = "https://api-ssl.bitly.com";
		url += "/v3/search?access_token="+BitlyKeys.ACCESS_TOKEN;
		
		url += "&query="+this.getQuery();
		
		if(!Utility.isEmpty(this.getLimit())){
			url += "&limit="+this.getLimit();
		}
		
		if(!Utility.isEmpty(this.getOffset())){
			url += "&offset="+this.getOffset();
		}
		
		if(!Utility.isEmpty(this.getLang())){
			url += "&lang="+this.getLang();
		}
		
		if(!Utility.isEmpty(this.getCities())){
			url += "&cities="+this.getCities();
		}
		
		if(!Utility.isEmpty(this.getDomain())){
			url += "&domain="+this.getDomain();
		}
		
		if(!Utility.isEmpty(this.getFull_domain())){
			url += "&full_domain="+this.getFull_domain();
		}
		
		if(!Utility.isEmpty(this.getFields())){
			url += "&fields="+this.getFields();
		}
		
		HTTPURLConnectionUtility urlUtility = new HTTPURLConnectionUtility();
		urlUtility.setUrl(url);
		urlUtility.setMethod(HTTPURLConnectionUtility.METHOD_GET);
		urlUtility.setDebug(isDebug());
		String response = urlUtility.getResponseData();
		
		if(this.isDebug()){
			//System.out.println(response.toString());
			System.out.println("===================Start: Filter Phrases====================");
		}
		JSONObject jObject  = new JSONObject(response);
		//If Response is positive
		if(jObject.get("status_txt")!=null && jObject.get("status_txt").toString().equals("OK")){
			//Get Data
			JSONObject data = jObject.getJSONObject("data");
			//Get Phrases
			JSONArray results = data.getJSONArray("results");
			
			if(results!=null && results.length()>0){
				for(int i=0;i<results.length();i++){
					JSONObject result = new JSONObject(results.get(i).toString());
					
					BitlyDataDTO bitlyData = new BitlyDataDTO();

					if(result.has("site") && result.get("site")!=null){
						bitlyData.setSite(result.get("site").toString());
					}
					if(result.has("last_indexed_epoch") && result.get("last_indexed_epoch")!=null){
						bitlyData.setLast_indexed_epoch(result.get("last_indexed_epoch").toString());
					}
					if(result.has("score") && result.get("score")!=null){
						bitlyData.setScore(result.get("score").toString());
					}
					if(result.has("initial_epoch") && result.get("initial_epoch")!=null){
						bitlyData.setInitial_epoch(result.get("initial_epoch").toString());
					}
					if(result.has("type") && result.get("type")!=null){
						bitlyData.setType(result.get("type").toString());
					}
					if(result.has("lang") && result.get("lang")!=null){
						bitlyData.setLang(result.get("lang").toString());
					}
					if(result.has("summaryText") && result.get("summaryText")!=null){
						bitlyData.setSummaryText(result.get("summaryText").toString());
					}
					if(result.has("url") && result.get("url")!=null){
						bitlyData.setUrl(result.get("url").toString());
					}
					if(result.has("lastseen") && result.get("lastseen")!=null){
						bitlyData.setLastseen(result.get("lastseen").toString());
					}
					if(result.has("content") && result.get("content")!=null){
						bitlyData.setContent(result.get("content").toString());
					}
					if(result.has("referrer") && result.get("referrer")!=null){
						bitlyData.setReferrer(result.get("referrer").toString());
					}
					if(result.has("title") && result.get("title")!=null){
						bitlyData.setTitle(result.get("title").toString());
					}
					if(result.has("summaryTitle") && result.get("summaryTitle")!=null){
						bitlyData.setSummaryText(result.get("summaryTitle").toString());
					}
					if(result.has("cities") && result.get("cities")!=null){
						bitlyData.setCities(result.get("cities").toString());
					}
					if(result.has("page") && result.get("page")!=null){
						bitlyData.setPage(result.get("page").toString());
					}
					if(result.has("initial") && result.get("initial")!=null){
						bitlyData.setInitial(result.get("initial").toString());
					}
					if(result.has("description") && result.get("description")!=null){
						bitlyData.setDescription(result.get("description").toString());
					}
					if(result.has("lastindexed") && result.get("lastindexed")!=null){
						bitlyData.setLastindexed(result.get("lastindexed").toString());
					}
					if(result.has("domain") && result.get("domain")!=null){
						bitlyData.setDomian(result.get("domain").toString());
					}
					if(result.has("aggregate_link") && result.get("aggregate_link")!=null){
						bitlyData.setAggregate_link(result.get("aggregate_link").toString());
					}
					if(result.has("h3") && result.get("h3")!=null){
						bitlyData.setH3(result.get("h3").toString());
					}
					if(result.has("h2") && result.get("h2")!=null){
						bitlyData.setH2(result.get("h2").toString());
					}
					if(result.has("ogtitle") && result.get("ogtitle")!=null){
						bitlyData.setOgtitle(result.get("ogtitle").toString());
					}
					if(result.has("aggregate_link") && result.get("aggregate_link")!=null){
						bitlyData.setAggregate_link(result.get("aggregate_link").toString());
					}
					
					bitlyDataList.add(bitlyData);
					
					//hotPhrases.add(phrase);
					//System.out.println(phrase.toString());
					//System.out.println(phrase.get("phrase"));
				}
			}
		}
		if(this.isDebug()){
			System.out.println("===================End: Filter Phrases====================");
		}
		
		}catch(Exception e){
			System.out.println(e);
		}
		return bitlyDataList;
		
		
		
	}
}
