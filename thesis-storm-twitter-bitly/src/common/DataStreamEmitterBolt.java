package common;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dto.BitlyDataDTO;
import dto.DataStream;

import twitter4j.Status;
import utility.Configuration;
import utility.DateStamp;
import utility.IConstants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class DataStreamEmitterBolt extends BaseRichBolt {
	private static final long serialVersionUID = 4150732456907856015L;
	private OutputCollector collector;
	
	//Date in string format, Data stream
	private HashMap<String,DataStream> dataStreams = new HashMap<String,DataStream>();
	
	//Assing Unique number if packes is completed
	private static int dataStreamPacketNumber = 1;
	
	
	@Override
	public void prepare(final Map stormConf, final TopologyContext context,
			final OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void execute(final Tuple input) {
		
		DataStream dataStream =  (DataStream) input.getValue(0);
		
		//--------------- Start - Prepare Data Stream List-----------------
		String dateStr = new SimpleDateFormat(DateStamp.DATE_TIME_FORMAT).format(dataStream.getTitleTimeWindowTime()); 
		DataStream dataStreamSaved = dataStreams.get(dateStr);
		if(dataStreamSaved==null){
			dataStreamSaved = dataStream;
			dataStreams.put(dateStr, dataStreamSaved);
		}else{
			
			//If in coming data streams is tiwtter data stream, it means saved data is bitly data stream in list against same time 
			if(dataStream.getDataStreamCategory()==IConstants.TWITTER_DATA_STREAM){
				//Copy Twitter data in saved list
				dataStreamSaved.setTwitterTitleTimeWindowTime(dataStream.getTwitterTitleTimeWindowTime());
				dataStreamSaved.setTwitterDataStream(dataStream.getTwitterDataStream());
			}

			//If in coming data streams is Bitly data stream, it means saved data is twitter data stream in list against same time 
			if(dataStream.getDataStreamCategory()==IConstants.BITLY_DATA_STREAM){
				//Copy Bitly data in saved list
				dataStreamSaved.setBitlyTitleTimeWindowTime(dataStream.getBitlyTitleTimeWindowTime());
				dataStreamSaved.setBitlyDataStream(dataStream.getBitlyDataStream());
				
			}
			
			if(dataStreamSaved.getTwitterDataStream()!=null && dataStreamSaved.getTwitterDataStream().size()>0 &&
					dataStreamSaved.getBitlyDataStream()!=null && dataStreamSaved.getBitlyDataStream().size()>0 &&
					dataStreamSaved.getTwitterTitleTimeWindowTime()!=null && dataStreamSaved.getBitlyTitleTimeWindowTime()!=null &&
					dataStreamSaved.getTwitterTitleTimeWindowTime().equals(dataStreamSaved.getBitlyTitleTimeWindowTime())){
				
				//OBject is completed
				dataStreamSaved.setDataStreamCategory(IConstants.TWITTER_BITLY_DATA_STREAM);//Mark that object is completed with both twiter and bitly streams
				dataStreamSaved.setDataStreamPacketNumber(dataStreamPacketNumber);
				dataStreamPacketNumber++;//For next completed packet
			}
		}
		//--------------- End - Prepare Data Stream List-----------------
		
		ArrayList<String> removalDates = new ArrayList<String>();
		
		//Emit Data Packet 
		if(dataStreams!=null && dataStreams.size()>0){
			for (Entry<String, DataStream> dataStreamByDate : dataStreams.entrySet()) {
				String date = dataStreamByDate.getKey();
				DataStream completeDataStream = dataStreamByDate.getValue();
				
				//If data stream is complete then emit it
				//If object contains both twitter and bitly streams with same title time window datetime
				if(completeDataStream.isTwitterAndBitlyDataStreamCompleted()){
					
					//Remove dates from map which are going to be emmitted
					removalDates.add(date);
					
					if(Configuration.PRINT_DATA_STREAM_TITLE_TIME_WINDOW){
						System.out.print(completeDataStream.toString());
					}

					collector.emit(input, new Values(completeDataStream));
					collector.ack(input);

				}
			}
		}
		
		//Remove data streams which are emitted.
		if(removalDates!=null && removalDates.size()>0){
			for(String date:removalDates){
				dataStreams.remove(date);
			}
		}
		

		
	}
	@Override
	public void declareOutputFields(final OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("client2"));
	}
}