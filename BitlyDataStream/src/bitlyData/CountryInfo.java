package bitlyData;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import dto.CountryInfoDTO;
import utility.BitlyKeys;
import utility.HTTPURLConnectionUtility;

public class CountryInfo {

	public ArrayList<CountryInfoDTO> getCountryInfo(String link, boolean debug){
		
		ArrayList<CountryInfoDTO> countries = new ArrayList<CountryInfoDTO>();
		try{
		String url = "https://api-ssl.bitly.com";
		url += "/v3/link/countries?access_token="+BitlyKeys.ACCESS_TOKEN+"&link="+link;
		
		HTTPURLConnectionUtility urlUtility = new HTTPURLConnectionUtility();
		urlUtility.setUrl(url);
		urlUtility.setMethod(HTTPURLConnectionUtility.METHOD_GET);
		String response = urlUtility.getResponseData();
		
		//System.out.println(response.toString());
		if(debug)
			System.out.println("===================Start: Country Info====================");
		
		JSONObject jObject  = new JSONObject(response);
		//If Response is positive
		if(jObject.get("status_txt")!=null && jObject.get("status_txt").toString().equals("OK")){
			//Get Data
			JSONObject data = jObject.getJSONObject("data");
			//Get Phrases
			JSONArray phrases = data.getJSONArray("countries");
			
			if(phrases!=null && phrases.length()>0){
				if(debug){
					System.out.println("==  Link Info ==");
					System.out.println("Link : " + link);
				}

				for(int i=0;i<phrases.length();i++){
					JSONObject phrase = new JSONObject(phrases.get(i).toString());
					
					if(phrase!=null && phrase.get("country")!=null){
						
						CountryInfoDTO countryInfo = new CountryInfoDTO();
						countryInfo.setClicks(getJSONAttribute(phrase,"clicks","").toString());
						countryInfo.setCountry(getJSONAttribute(phrase,"country","").toString());
						countries.add(countryInfo);
						
						if(debug){
							System.out.println("Clicks : " + getJSONAttribute(phrase,"clicks","").toString());
							System.out.println("Country : " + getJSONAttribute(phrase,"country","").toString());
						}
					}
					
					//System.out.println(phrase.toString());
					//System.out.println(phrase.get("link"));
					//String link = phrase.get("link").toString();
					
				}
				if(debug){
					System.out.println("== End Link Info ==");
				}

			}
		}
		
		if(debug)
			System.out.println("===================End: Country Info====================");
		
		}catch(Exception e){
			System.out.println(e);
		}
		
		return countries;
		
	}
	
	private Object getJSONAttribute(JSONObject jsonObj, String attribute, Object defaultValue){
		try{
			return jsonObj.get(attribute);
		}catch(Exception e){
		    return 	defaultValue;
		}
	}
}
