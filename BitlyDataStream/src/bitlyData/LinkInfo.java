package bitlyData;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import dto.CountryInfoDTO;
import dto.LinkInfoDTO;
import utility.BitlyKeys;
import utility.HTTPURLConnectionUtility;

public class LinkInfo {

	public LinkInfoDTO getLinkInfo(String link, boolean setCountry, boolean debug){
		
		LinkInfoDTO linkInfo = null;
		
		try{
		String url = "https://api-ssl.bitly.com";
		url += "/v3/link/info?access_token="+BitlyKeys.ACCESS_TOKEN+"&link="+link;
		
		HTTPURLConnectionUtility urlUtility = new HTTPURLConnectionUtility();
		urlUtility.setUrl(url);
		urlUtility.setMethod(HTTPURLConnectionUtility.METHOD_GET);
		String response = urlUtility.getResponseData();
		
		//System.out.println(response.toString());
		if(debug)
			System.out.println("===================Start: Link Info====================");
		
		JSONObject jObject  = new JSONObject(response);
		//If Response is positive
		if(jObject.get("status_txt")!=null && jObject.get("status_txt").toString().equals("OK")){
			//Get Data
			JSONObject data = jObject.getJSONObject("data");
			
			if(data != null){
				
				if(debug){
					System.out.println("Link : " + link);
					System.out.println(data.toString());
				}
				
				linkInfo = new LinkInfoDTO();
				linkInfo.setCanonicalUrl(getJSONAttribute(data,"canonical_url","").toString());
				linkInfo.setCategory(getJSONAttribute(data,"category","").toString());
				linkInfo.setContentLength(getJSONAttribute(data,"content_length","").toString());
				linkInfo.setContentType(getJSONAttribute(data,"content_type","").toString());
				linkInfo.setDomain(getJSONAttribute(data,"domain","").toString());
				linkInfo.setFaviconUrl(getJSONAttribute(data,"favicon_url","").toString());
				linkInfo.setGlobalHash(getJSONAttribute(data,"global_hash","").toString());
				linkInfo.setHtmlTitle(getJSONAttribute(data,"html_title","").toString());
				linkInfo.setHttpCode(getJSONAttribute(data,"http_code","").toString());
				linkInfo.setIndexed(getJSONAttribute(data,"indexed","").toString());
				linkInfo.setOriginalUrl(getJSONAttribute(data,"original_url","").toString());
				linkInfo.setRobotsAllowed(getJSONAttribute(data,"robots_allowed","").toString());
				linkInfo.setSourceDomain(getJSONAttribute(data,"source_domain","").toString());
				linkInfo.setUrl(getJSONAttribute(data,"url","").toString());
				linkInfo.setUrlFetched(getJSONAttribute(data,"url_fetched","").toString());
				
				JSONArray linktags = getJSONArrayAttribute(data,"linktags_other",null);
				ArrayList<String> linkTagsOthers = new ArrayList<String>();
				if(linktags!=null && linktags.length()>0){
					for(int i=0;i<linktags.length();i++){
						linkTagsOthers.add(linktags.get(i).toString());
					}
				}
				linkInfo.setLinktagsOthers(linkTagsOthers);

				JSONArray metatags = getJSONArrayAttribute(data,"metatags_name",null);
				ArrayList<String> metaTagNames = new ArrayList<String>();
				if(metatags!=null && metatags.length()>0){
					for(int i=0;i<metatags.length();i++){
						metaTagNames.add(metatags.get(i).toString());
					}
				}
				linkInfo.setMetatagsNames(metaTagNames);
				
				if(setCountry){
					CountryInfo countryInfo = new CountryInfo();
					ArrayList<CountryInfoDTO> countries = countryInfo.getCountryInfo(link, false);
					linkInfo.setCountries(countries); 
					
				}
			}
			
//			//Get Phrases
//			JSONArray phrases = data.getJSONArray("popular_links");
//			
//			if(phrases!=null && phrases.length()>0){
//				for(int i=0;i<phrases.length();i++){
//					JSONObject phrase = new JSONObject(phrases.get(i).toString());
//					//System.out.println(phrase.toString());
//					//System.out.println(phrase.get("link"));
//					//String link = phrase.get("link").toString();
//					
//				}
//			}
		}
		
		if(debug)
			System.out.println("===================End: Link Info ====================");
		
		}catch(Exception e){
			System.out.println(e);
		}
		
		return linkInfo;
		
		
	}
	
	private Object getJSONAttribute(JSONObject jsonObj, String attribute, Object defaultValue){
		try{
			return jsonObj.get(attribute);
		}catch(Exception e){
		    return 	defaultValue;
		}
	}
	
	private JSONArray getJSONArrayAttribute(JSONObject jsonObj, String attribute, JSONArray defaultValue){
		try{
			return jsonObj.getJSONArray(attribute);
		}catch(Exception e){
		    return 	defaultValue;
		}
	}
}
