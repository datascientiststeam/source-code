package NLP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import dto.Document;

public class DocumentsTFIDF {
	
	ArrayList<Document> documets = new ArrayList<Document>();
	
	//String (Unique Word) // Count : (Number of times word appears in all documents)
	HashMap<String,Integer> wordCountMap = new HashMap<String,Integer>(); 
	
	//String (Unique Word) // HashMap<Document,Integer> Document // Integer : Always 1 (HashMap represent Number of document contains word)
	HashMap<String,HashMap<Document,Integer>> numberOfDocumentWordContainsMap = new HashMap<String,HashMap<Document,Integer>>();
	
	HashMap<String,HashMap<Document,Double>> TF = new HashMap<String,HashMap<Document,Double>>(); 
	HashMap<String,Double> IDF = new  HashMap<String,Double>(); 
	HashMap<String,HashMap<Document,Double>> TF_IDF = new HashMap<String,HashMap<Document,Double>>(); 
	
	
	public void addDocument(Document document){
		documets.add(document);
	}
	
	public void prepareTFIDFWeights(){
		prepareUniqueWordCount();
		prepareTF();
		prepareIDF();
		prepareTFIDF();
	}
	
	private void prepareUniqueWordCount(){
		if(documets!=null && documets.size()>0){
			
			for(Document document : documets){
				HashMap<String,Integer> wordCoutDocumentMap = document.getWordCountMap();
				if(wordCoutDocumentMap!=null){
					Iterator it = wordCoutDocumentMap.entrySet().iterator();
				    while (it.hasNext()) {
				    	
				        Map.Entry pairs = (Map.Entry)it.next();
				        String word = (String) pairs.getKey();
				        Integer numberOfTimesWordAppears = (Integer) pairs.getValue();//Number of time a word appears in document
				        
				        //String (Unique Word) // Count : (Number of times word appears in all documents)
				        Integer count = wordCountMap.get(word);
				        if(count==null){
							count = new Integer(numberOfTimesWordAppears.intValue());
						}else{
							count += numberOfTimesWordAppears.intValue(); 
						}
						wordCountMap.put(word, count);
						
						
						//String (Unique Word) // HashMap<Document,Integer> Document // Integer : Always 1 (HashMap represent Number of document contains word)
						HashMap<Document,Integer> documentCountMap = numberOfDocumentWordContainsMap.get(word);
				        if(documentCountMap==null){
				        	documentCountMap = new HashMap<Document,Integer>();
				        	numberOfDocumentWordContainsMap.put(word, documentCountMap);
						}
				        Integer documentCount = documentCountMap.get(document);
				        if(documentCount==null){
				        	documentCountMap.put(document, 1);
				        }
				        
				        
				    }
				}
			}
		}
	}
	
	private void prepareTF(){
		if(wordCountMap!=null && wordCountMap.size()>0){
			
			//Unique Words Map
			Iterator it = wordCountMap.entrySet().iterator();
		    while (it.hasNext()) {
		    	
		        Map.Entry pairs = (Map.Entry)it.next();
		        String word = (String) pairs.getKey();
		        Integer count = (Integer) pairs.getValue();//Number of document contains word (term)
		        
		        
		        if(documets!=null && documets.size()>0){

		        	//Iterate All documents
					for(Document document : documets){
						
						Integer numberOfTimesWordAppears = 0;
						
						//Word and Its count map within document
						HashMap<String,Integer> wordCoutDocumentMap = document.getWordCountMap();
						if(wordCoutDocumentMap!=null){
							numberOfTimesWordAppears = wordCoutDocumentMap.get(word);
							
							//If document not contain word
							if(numberOfTimesWordAppears==null){
								numberOfTimesWordAppears = 0;
							}
						}
						
						/**************************************Start : TF Map *********************/
				        
				        Double TFValue = 0.0;
				        if(numberOfTimesWordAppears!=null && wordCoutDocumentMap!=null){
				        	//Calculation TF value
				        	TFValue = TFIDF_Utility.getTF(numberOfTimesWordAppears, wordCoutDocumentMap.size());
				        }

				        //Get Term Frequency By Document
				        HashMap<Document,Double> termFrequencyByDocument = TF.get(word);
				        if(termFrequencyByDocument==null){
				        	termFrequencyByDocument = new HashMap<Document,Double>();
				        	TF.put(word, termFrequencyByDocument);
				        }
				        termFrequencyByDocument.put(document, TFValue);
				        
				        /**************************************End : TF Map *********************/
				        
							
					}
		        }
		    }
		}
	}
	
	private void prepareIDF(){
		
		if(wordCountMap!=null && wordCountMap.size()>0){
			
			//Unique Words Map
			Iterator it = wordCountMap.entrySet().iterator();
		    while (it.hasNext()) {
		    	
		        Map.Entry pairs = (Map.Entry)it.next();
		        String word = (String) pairs.getKey();
		        Integer count = (Integer) pairs.getValue();//Number of document contains word (term)
		        
		        
		        Integer numberOfDocumenetContainsWord = 0;
		        if(numberOfDocumentWordContainsMap!=null && numberOfDocumentWordContainsMap.size()>0){
		        	HashMap<Document,Integer> documentCountMap = numberOfDocumentWordContainsMap.get(word);
		        	if(documentCountMap!=null){
		        		numberOfDocumenetContainsWord = documentCountMap.size();
		        	}
		        }
		        
		        Double idfCal = 0.0;
		        if(documets!=null && documets.size()>0){
		        	idfCal = TFIDF_Utility.getIDF(documets.size(), numberOfDocumenetContainsWord);
		        }
		        IDF.put(word, idfCal);
		        
		        
		        
		    }
		}
	}
	
	public void prepareTFIDF(){
		
		if(wordCountMap!=null && wordCountMap.size()>0){
					
			//Unique Words Map
			Iterator it = wordCountMap.entrySet().iterator();
		    while (it.hasNext()) {
		    	
		        Map.Entry pairs = (Map.Entry)it.next();
		        String word = (String) pairs.getKey();
		        Integer count = (Integer) pairs.getValue();//Number of document contains word (term)
		        
		        if(documets!=null && documets.size()>0){

		        	//Iterate All documents
					for(Document document : documets){
						
						//GET TF
						Double TFValue = 0.0;
						HashMap<Document,Double> TFDocument = TF.get(word);
						if(TFDocument!=null){
							TFValue = TFDocument.get(document);
						}
						
						//GET IDF
						Double IDFValue = IDF.get(word);
						
						//Get TF-IDF value
				        Double TFIDFValue = 0.0;
				        if(IDFValue!=null && TFValue!=null){
				        	TFIDFValue = TFValue * IDFValue; 
				        }
						
						
						HashMap<Document,Double> TFIDFByDocument = TF_IDF.get(word);
				        if(TFIDFByDocument==null){
				        	TFIDFByDocument = new HashMap<Document,Double>();
				        	TF_IDF.put(word, TFIDFByDocument);
				        }
				        TFIDFByDocument.put(document, TFIDFValue);
						
						
					}
		        }
		        
		    }
		}
	}
	public void printMaps(){
		System.out.println("---------------------- Start : Documents ---------------------------------");
		if(documets!=null && documets.size()>0){
			int i=0;
			for(Document document:documets){
				System.out.println("Doc " + (++i) + " : " + document);
			}
		}
		System.out.println("---------------------- End : Documents ---------------------------------");
		System.out.println("---------------------- Start : Unique Words and its count in all documents ---------------------------------");
		if(wordCountMap!=null && wordCountMap.size()>0){
			
			//Unique Words Map
			Iterator it = wordCountMap.entrySet().iterator();
		    while (it.hasNext()) {
		    	
		        Map.Entry pairs = (Map.Entry)it.next();
		        String word = (String) pairs.getKey();
		        Integer count = (Integer) pairs.getValue();//Number of document contains word (term)
		        
		        System.out.println(word + " - " + count);
		    }
		}
		System.out.println("---------------------- End : Unique Words and its count in all documents ---------------------------------");
		System.out.println("---------------------- Start : Number of documents contains by word ---------------------------------");
		if(numberOfDocumentWordContainsMap!=null && numberOfDocumentWordContainsMap.size()>0){
			
			//Unique Words Map
			Iterator it = numberOfDocumentWordContainsMap.entrySet().iterator();
		    while (it.hasNext()) {
		    	
		        Map.Entry pairs = (Map.Entry)it.next();
		        String word = (String) pairs.getKey();
		        HashMap<Document,Integer> value = (HashMap<Document,Integer>) pairs.getValue();//Number of document contains word (term)
		        
		        System.out.println(word + " - " + value);
		    }
		}
		System.out.println("---------------------- End : Unique Words and its count in all documents ---------------------------------");
		System.out.println("---------------------- Start : TF ---------------------------------");
		if(TF!=null && TF.size()>0){
			
			//Unique Words Map
			Iterator it = TF.entrySet().iterator();
		    while (it.hasNext()) {
		    	
		        Map.Entry pairs = (Map.Entry)it.next();
		        String word = (String) pairs.getKey();
		        HashMap<Document,Double> value = (HashMap<Document,Double>) pairs.getValue();//Number of document contains word (term)
		        
		        System.out.println(word);
		        
		        //Unique Words Map
				Iterator it2 = value.entrySet().iterator();
			    while (it2.hasNext()) {
			    	
			        Map.Entry pairs2 = (Map.Entry)it2.next();
			        Document document = (Document) pairs2.getKey();
			        Double tfvalue = (Double) pairs2.getValue();//Number of document contains word (term)
			        
			        System.out.println("         "+document + " - " + tfvalue);
			    }
		        
		        
		    }
		}
		System.out.println("---------------------- End : TF ---------------------------------");
		System.out.println("---------------------- Start : IDF ---------------------------------");
		if(IDF!=null && IDF.size()>0){
			
			//Unique Words Map
			Iterator it = IDF.entrySet().iterator();
		    while (it.hasNext()) {
		    	
		        Map.Entry pairs = (Map.Entry)it.next();
		        String word = (String) pairs.getKey();
		        Double value = (Double) pairs.getValue();//Number of document contains word (term)
		        
		        System.out.println(word + " - " + value);
			    
		        
		    }
		}
		System.out.println("---------------------- End : IDF ---------------------------------");
		System.out.println("---------------------- Start : TF-IDF ---------------------------------");
		if(TF_IDF!=null && TF_IDF.size()>0){
			
			//Unique Words Map
			Iterator it = TF_IDF.entrySet().iterator();
		    while (it.hasNext()) {
		    	
		        Map.Entry pairs = (Map.Entry)it.next();
		        String word = (String) pairs.getKey();
		        HashMap<Document,Double> value = (HashMap<Document,Double>) pairs.getValue();//Number of document contains word (term)
		        
		        System.out.println(word);
		        
		        //Unique Words Map
				Iterator it2 = value.entrySet().iterator();
			    while (it2.hasNext()) {
			    	
			        Map.Entry pairs2 = (Map.Entry)it2.next();
			        Document document = (Document) pairs2.getKey();
			        Double tfIDFvalue = (Double) pairs2.getValue();//Number of document contains word (term)
			        
			        System.out.println("         "+document + " - " + tfIDFvalue);
			    }
		        
		        
		    }
		}
		System.out.println("---------------------- End : TF-IDF ---------------------------------");
	}
	
}
