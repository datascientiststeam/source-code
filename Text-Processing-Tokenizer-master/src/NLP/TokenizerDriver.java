package NLP;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

import dto.Document;

import snowball.ext.englishStemmer;

/**
 * @author brandonskane a text processing module as follows: A tokenizer
 *         according to the following rules: Tokenize all abbreviations
 *         containing periods as strings without periods Treat the rest of the
 *         punctuation as word separators Lowercase all letters
 * 
 *         A stemmer implemented via the Porter Stemmer package:
 *         http://tartarus.org/~martin/PorterStemmer/ Implement stopword removal
 *         using the following stopword list: http://bit.ly/1bqQWaV
 * 
 *         The word list is tokenized first, stopwords are removed, and finally
 *         the words are put through the stemmer
 */
public class TokenizerDriver {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		
		try{
			//https://github.com/brandonskane/Text-Processing-Tokenizer
	
//			System.out.println("Text Processing Tokenizer");
//			System.out.println("-------------------------");
//	
//			String fileName = "src/text.txt";
//			String stopWordsFileName = "src/stopwords.txt";
//	
//			String rawInput = captureText(fileName);
//			String stopWordsString = processStopWords(stopWordsFileName);
//	
//			System.out.println(rawInput);
//	
//			String[] seperatedWords = rawInput.split(" ");
//			String[] stopWords = stopWordsString.split("\n");
//	
//			Tokenizer tokenizer = new Tokenizer(seperatedWords, stopWords);
//	
//			//tokenizer.stemWords(new Stemmer()); //Custom Stemmer
//			tokenizer.stemWords(new englishStemmer()); // Snowball Stemmer
//			String tokenizedWords = tokenizer.getProcessedWords();
//	
//			System.out.println();
//			System.out.println("Text Processing Tokenizer");
//			System.out.println("-------------------------");
//			System.out.println(tokenizedWords);
			
			System.out.println("Text Processing Tokenizer");
			System.out.println("-------------------------");
	
			String fileName = "src/doc1.txt";
			String rawInput = captureText(fileName);
			System.out.println(rawInput);
			Document doc1 = new Document(rawInput);
			doc1.tockenizeDocument();
			
			fileName = "src/doc2.txt";
			rawInput = captureText(fileName);
			System.out.println(rawInput);
			Document doc2 = new Document(rawInput);
			doc2.tockenizeDocument();

//			fileName = "src/doc3.txt";
//			rawInput = captureText(fileName);
//			System.out.println(rawInput);
//			Document doc3 = new Document(rawInput);
//			doc3.tockenizeDocument();

			
			DocumentsTFIDF TFIDFDocs = new DocumentsTFIDF();
			TFIDFDocs.addDocument(doc1);
			TFIDFDocs.addDocument(doc2);
//			TFIDFDocs.addDocument(doc3);
			TFIDFDocs.prepareTFIDFWeights();
			TFIDFDocs.printMaps();
	
	
			System.out.println();
			System.out.println("Text Processing Tokenizer");
			System.out.println("-------------------------");
			//System.out.println(tokenizedWords);
		}catch(Exception e){
			System.out.println(e);
		}

	}

	static String captureText(String fileName) throws IOException {
		String textToProcess;
		FileInputStream inputStream = new FileInputStream(fileName);
		try {
			textToProcess = IOUtils.toString(inputStream);
		} finally {
			inputStream.close();
		}
		return textToProcess;
	}

	static String processStopWords(String fileName) throws IOException {
		String stopWords;
		FileInputStream inputStream = new FileInputStream(fileName);
		try {
			stopWords = IOUtils.toString(inputStream);
		} finally {
			inputStream.close();
		}
		return stopWords;

	}
}
