package dto;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;

import snowball.ext.englishStemmer;

import NLP.*;

public class Document extends Tokenizer {
	
	private String rawInput;
	private ArrayList<String> orignalInput = new ArrayList<String>();
	private HashMap<String,Integer> wordCount = new HashMap<String,Integer>(); 
	
	public Document(String rawInput){
		this.rawInput = rawInput;
		
		String[] seperatedWords = rawInput.split(" ");
		for(String str: seperatedWords){
			orignalInput.add(str);
		}
	}
	

	public void tockenizeDocument() throws IOException{
		String[] seperatedWords = rawInput.split(" ");
		tokenizer(seperatedWords);
		removeEmptyWords();
		removeStopWords();
		stemming();
		prepareWordCountMap();
	}
	
	private void removeStopWords() throws IOException{
		String stopWordsString = captureText(IConstants.STOP_WORDS_FILE_PATH);
		String[] stopWords = stopWordsString.split("\n");
		removeStopWords(stopWords);
	}
	
	private void stemming(){
		//tokenizer.stemWords(new Stemmer()); //Custom Stemmer
		stemWords(new englishStemmer()); // Snowball Stemmer
	}
	
	public void prepareWordCountMap(){

		ArrayList<String> proccessedWord = getProcessedWordsArray();
		for (String word : proccessedWord) {
			
			Integer count = wordCount.get(word);
			if(count==null){
				count = new Integer(1);
			}else{
				count++;
			}
			wordCount.put(word, count);
		}

	}
	
	public HashMap<String,Integer> getWordCountMap(){
		return wordCount;
	}
	
	
	
	
	static String captureText(String fileName) throws IOException {
		String textToProcess;
		FileInputStream inputStream = new FileInputStream(fileName);
		try {
			textToProcess = IOUtils.toString(inputStream);
		} finally {
			inputStream.close();
		}
		return textToProcess;
	}
	
	
}
