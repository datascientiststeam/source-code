package account;
/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Gets account settings.
 *
 * @author Yusuke Yamamoto - yusuke at mac.com
 */
public final class GetAccountSettings {
    /**
     * Usage: java twitter4j.examples.account.GetAccountSettings
     *
     * @param args arguments doesn't take effect with this example
     */
    public static void main(String[] args) {
        try {
            
            ConfigurationBuilder cb = new ConfigurationBuilder();
            cb.setDebugEnabled(true)
              .setOAuthConsumerKey("4wLH6JDTxLsWIfIju9CyOdy34")
              .setOAuthConsumerSecret("fx1EDOSVzjOvaMeARpNF4dC8foNDCuWBgyz1lpwkgFoxqeQEwA")
              .setOAuthAccessToken("548443812-CtGmilyt78DMBc4btgry0M5JYMzCf5SaPrzOGEDv")
              .setOAuthAccessTokenSecret("8KK1Z2mayDEvviT9ytaVc3zHZ6jwozaokMetSm638rvnL");
            TwitterFactory tf = new TwitterFactory(cb.build());
            Twitter twitter = tf.getInstance();
            
            //Twitter twitter = new TwitterFactory().getInstance();
            AccountSettings settings = twitter.getAccountSettings();
            
            
            System.out.println("Sleep time enabled: " + settings.isSleepTimeEnabled());
            System.out.println("Sleep end time: " + settings.getSleepEndTime());
            System.out.println("Sleep start time: " + settings.getSleepStartTime());
            System.out.println("Geo enabled: " + settings.isGeoEnabled());
            System.out.println("Screen name: " + settings.getScreenName());
            System.out.println("Listing trend locations:");
            Location[] locations = settings.getTrendLocations();
            for (Location location : locations) {
                System.out.println(" " + location.getName());
            }
            System.exit(0);
        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to get account settings: " + te.getMessage());
            System.exit(-1);
        }
    }
}
