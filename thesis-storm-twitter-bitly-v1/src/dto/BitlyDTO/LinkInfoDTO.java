package dto.BitlyDTO;

import java.util.ArrayList;

public class LinkInfoDTO {

	private String canonicalUrl;
	private String category;
	private String contentLength;
	private String contentType;
	private String domain;
	private String FaviconUrl;
	private String globalHash;
	private String htmlTitle;
	private String httpCode;
	private String indexed;
	private ArrayList<String> linktagsOthers;
	private ArrayList<String> metatagsNames;
	private String originalUrl;
	private String robotsAllowed;
	private String sourceDomain;
	private String url;
	private String urlFetched;
	private ArrayList<CountryInfoDTO> countries;
	
	public String getCanonicalUrl() {
		return canonicalUrl;
	}
	public void setCanonicalUrl(String canonicalUrl) {
		this.canonicalUrl = canonicalUrl;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getContentLength() {
		return contentLength;
	}
	public void setContentLength(String contentLength) {
		this.contentLength = contentLength;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getFaviconUrl() {
		return FaviconUrl;
	}
	public void setFaviconUrl(String faviconUrl) {
		FaviconUrl = faviconUrl;
	}
	public String getGlobalHash() {
		return globalHash;
	}
	public void setGlobalHash(String globalHash) {
		this.globalHash = globalHash;
	}
	public String getHtmlTitle() {
		return htmlTitle;
	}
	public void setHtmlTitle(String htmlTitle) {
		this.htmlTitle = htmlTitle;
	}
	public String getHttpCode() {
		return httpCode;
	}
	public void setHttpCode(String httpCode) {
		this.httpCode = httpCode;
	}
	public String getIndexed() {
		return indexed;
	}
	public void setIndexed(String indexed) {
		this.indexed = indexed;
	}
	public ArrayList<String> getLinktagsOthers() {
		return linktagsOthers;
	}
	public void setLinktagsOthers(ArrayList<String> linktagsOthers) {
		this.linktagsOthers = linktagsOthers;
	}
	public ArrayList<String> getMetatagsNames() {
		return metatagsNames;
	}
	public void setMetatagsNames(ArrayList<String> metatagsNames) {
		this.metatagsNames = metatagsNames;
	}
	public String getOriginalUrl() {
		return originalUrl;
	}
	public void setOriginalUrl(String originalUrl) {
		this.originalUrl = originalUrl;
	}
	public String getRobotsAllowed() {
		return robotsAllowed;
	}
	public void setRobotsAllowed(String robotsAllowed) {
		this.robotsAllowed = robotsAllowed;
	}
	public String getSourceDomain() {
		return sourceDomain;
	}
	public void setSourceDomain(String sourceDomain) {
		this.sourceDomain = sourceDomain;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrlFetched() {
		return urlFetched;
	}
	public void setUrlFetched(String urlFetched) {
		this.urlFetched = urlFetched;
	}
	public ArrayList<CountryInfoDTO> getCountries() {
		return countries;
	}
	public void setCountries(ArrayList<CountryInfoDTO> countries) {
		this.countries = countries;
	}
	
	public boolean isPakistanCountryInfo(){
		if(countries!=null && countries.size()>0){
			for(CountryInfoDTO c : countries){
				if(c.getCountry().toLowerCase().equals("pk") || 
						c.getCountry().toLowerCase().contains("pak")){
					return true;
				}
			}
			
		}
		return false;
	}
	
	public String getCountriesStr(){
		String countriesStr = "";
		if(countries!=null && countries.size()>0){
			for(CountryInfoDTO c : countries){
				countriesStr += c.getCountry() + "  ";
			}
		}
		return countriesStr;
	}
}
