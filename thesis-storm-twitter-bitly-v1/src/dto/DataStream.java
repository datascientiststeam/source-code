package dto;

import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

import dto.BitlyDTO.LinkInfoDTO;
import twitter4j.Status;
import utility.GlobalTime;
import utility.IConstants;

public class DataStream {
	
	private int dataStreamCategory;//Show which data stream (Twitter/Bitly)
	private Date titleTimeWindowTime;
	private LinkedBlockingQueue<Status> twitterDataStream;
	private HashMap<String,LinkInfoDTO> bitlyDataStream;
	private Date twitterTitleTimeWindowTime;
	private Date bitlyTitleTimeWindowTime;
	private int dataStreamPacketNumber;//It will be assing by DataSteamEmitterBolt when data stream will be completed
	
	public int getDataStreamCategory() {
		return dataStreamCategory;
	}
	public void setDataStreamCategory(int dataStreamCategory) {
		this.dataStreamCategory = dataStreamCategory;
	}
	public Date getTitleTimeWindowTime() {
		return titleTimeWindowTime;
	}
	public void setTitleTimeWindowTime(Date titleTimeWindowTime) {
		this.titleTimeWindowTime = titleTimeWindowTime;
	}
	public LinkedBlockingQueue<Status> getTwitterDataStream() {
		return twitterDataStream;
	}
	public void setTwitterDataStream(LinkedBlockingQueue<Status> twitterDataStream) {
		this.twitterDataStream = twitterDataStream;
	}
	public HashMap<String, LinkInfoDTO> getBitlyDataStream() {
		return bitlyDataStream;
	}
	public void setBitlyDataStream(HashMap<String, LinkInfoDTO> bitlyDataStream) {
		this.bitlyDataStream = bitlyDataStream;
	}
	
	public Date getTwitterTitleTimeWindowTime() {
		return twitterTitleTimeWindowTime;
	}
	public void setTwitterTitleTimeWindowTime(Date twitterTitleTimeWindowTime) {
		this.twitterTitleTimeWindowTime = twitterTitleTimeWindowTime;
	}
	public Date getBitlyTitleTimeWindowTime() {
		return bitlyTitleTimeWindowTime;
	}
	public void setBitlyTitleTimeWindowTime(Date bitlyTitleTimeWindowTime) {
		this.bitlyTitleTimeWindowTime = bitlyTitleTimeWindowTime;
	}
	
	public boolean isTwitterAndBitlyDataStreamCompleted(){
		//If Data stream contains both category, Twitter and Bitly
		if(this.getDataStreamCategory()==IConstants.TWITTER_BITLY_DATA_STREAM){
			
			//If both twitter and bitly stream is not null and title time window is same of both streams then stream is completed
			if(this.getTwitterDataStream()!=null && this.getBitlyDataStream()!=null && 
					this.getTwitterTitleTimeWindowTime()!=null && this.getBitlyTitleTimeWindowTime()!=null &&
					this.getTwitterTitleTimeWindowTime().equals(this.getBitlyTitleTimeWindowTime())
					){
				return true;
				
			}
		}
		
		return false;
	}
	public int getDataStreamPacketNumber() {
		return dataStreamPacketNumber;
	}
	public void setDataStreamPacketNumber(int dataStreamPacketNumber) {
		this.dataStreamPacketNumber = dataStreamPacketNumber;
	}
	
	
	public String toString(){
		StringBuffer str = new StringBuffer();
		if(this.getTwitterDataStream()!=null){

			LinkedBlockingQueue<Status> statusQueues = this.getTwitterDataStream();
			str.append("-------------------- Start : Twitter Stream  " + this.getTitleTimeWindowTime() +" ----------------------\n");
			if(statusQueues!=null && statusQueues.size()>0){
				for(Status q : statusQueues){
					str.append("@" + q.getUser().getScreenName() + " - " + q.getText() + "\n");
				}
			}
			str.append("-------------------- End : Twitter Stream ----------------------\n");

		}
		
		if(this.getBitlyDataStream()!=null){
			
			HashMap<String,LinkInfoDTO> statusQueues = this.getBitlyDataStream();
			str.append("-------------------- Start : Bitly Stream  " + this.getBitlyTitleTimeWindowTime() +" ----------------------\n");
			if(statusQueues!=null && statusQueues.size()>0){
				for(LinkInfoDTO q : statusQueues.values()){
					str.append("@" + " - " + q.getHtmlTitle() + " - " + "" + " - "+ q.getCountriesStr() + "\n");
				}
			}
			
			str.append("-------------------- End : Bitly Stream ----------------------\n");
		}
		
		return str.toString();
	}
	
	
}
