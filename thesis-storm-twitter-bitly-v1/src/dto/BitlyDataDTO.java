package dto;

public class BitlyDataDTO {
	
	private String domian;
	private String initial_epoch;
	private String h2;
	private String h3;
	private String site;
	private String lastindexed;
	private String keywords;
	private String last_indexed_epoch;
	private String title;
	private String initial;
	private String summaryText;
	private String content;
	private String score;
	private String summaryTitle;
	private String type;
	private String description;
	private String cities;
	private String lang;
	private String url;
	private String referrer;
	private String aggregate_link;
	private String lastseen;
	private String page;
	private String ogtitle;
	
	public String getDomian() {
		return domian;
	}
	public void setDomian(String domian) {
		this.domian = domian;
	}
	public String getInitial_epoch() {
		return initial_epoch;
	}
	public void setInitial_epoch(String initial_epoch) {
		this.initial_epoch = initial_epoch;
	}
	public String getH2() {
		return h2;
	}
	public void setH2(String h2) {
		this.h2 = h2;
	}
	public String getH3() {
		return h3;
	}
	public void setH3(String h3) {
		this.h3 = h3;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getLastindexed() {
		return lastindexed;
	}
	public void setLastindexed(String lastindexed) {
		this.lastindexed = lastindexed;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getLast_indexed_epoch() {
		return last_indexed_epoch;
	}
	public void setLast_indexed_epoch(String last_indexed_epoch) {
		this.last_indexed_epoch = last_indexed_epoch;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getInitial() {
		return initial;
	}
	public void setInitial(String initial) {
		this.initial = initial;
	}
	public String getSummaryText() {
		return summaryText;
	}
	public void setSummaryText(String summaryText) {
		this.summaryText = summaryText;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getSummaryTitle() {
		return summaryTitle;
	}
	public void setSummaryTitle(String summaryTitle) {
		this.summaryTitle = summaryTitle;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCities() {
		return cities;
	}
	public void setCities(String cities) {
		this.cities = cities;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getReferrer() {
		return referrer;
	}
	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}
	public String getAggregate_link() {
		return aggregate_link;
	}
	public void setAggregate_link(String aggregate_link) {
		this.aggregate_link = aggregate_link;
	}
	public String getLastseen() {
		return lastseen;
	}
	public void setLastseen(String lastseen) {
		this.lastseen = lastseen;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getOgtitle() {
		return ogtitle;
	}
	public void setOgtitle(String ogtitle) {
		this.ogtitle = ogtitle;
	}
	
	
}
