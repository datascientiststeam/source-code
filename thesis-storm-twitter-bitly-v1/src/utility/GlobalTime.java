package utility;

import java.util.Date;



public class GlobalTime  implements Runnable {

	private static Date startDate;
	public static Date nextTitleTimeWindowDateTime;

    public GlobalTime(Date currentDate) {
        this.startDate = currentDate;
        nextTitleTimeWindowDateTime = currentDate;
    }

	
	@Override
	public void run() {
		extrentTileTimeWindowTime();
		
		// TODO Auto-generated method stub
		
	}
	
	public static void extrentTileTimeWindowTime(){
	
		if(Configuration.DEBUG_TITLE_TIME_WINDOW_TIMEDATE){
			System.out.println("---------------Start Title Time Window Time : " + nextTitleTimeWindowDateTime + "--------------------------");
		}
		
		//Extent time
		final long ONE_MINUTE_IN_MILLIS=60000;//millisecs
		long delay = IConstants.TITLE_TIME_WINDOW_IN_MINUTES * ONE_MINUTE_IN_MILLIS;
		long t=nextTitleTimeWindowDateTime.getTime();
		nextTitleTimeWindowDateTime=new Date(t + (delay));
		
		if(Configuration.DEBUG_TITLE_TIME_WINDOW_TIMEDATE){
			System.out.println("-----------------------Next Title Time Window : " + nextTitleTimeWindowDateTime +" ------------------------");
		}
		
		
		
		new java.util.Timer().schedule( 
		        new java.util.TimerTask() {
		            @Override
		            public void run() {
		                // your code here
		            	//Extent time
		            	GlobalTime.extrentTileTimeWindowTime();
		            	
		        	
		            }
		        }, 
		        delay 
		);
		
	}

}
