package utility;

import bitlyData.FilterPhrases;
import twitter4j.FilterQuery;

public class Configuration {

	//Debug
	public static boolean DEBUG_TWITTER_STREAM = true;//Show one by one stream data
	public static boolean DEBUG_BITLY_STREAM = false;//Show one by one stream data
	public static boolean DEBUG_TITLE_TIME_WINDOW_TIMEDATE = true;
	public static boolean PRINT_DATA_STREAM_TITLE_TIME_WINDOW = true;//Print data streams contains in title time window
	public static boolean PRINT_TF_IDF_MAP_EACH_CRITERIA = true;
	public static boolean PRINT_TF_IDF_MAP_EACH_SUCESS_CONSINE_SIMILARITY_CRITERIA = true;
	public static boolean PRINT_CONSINE_SIMILARITY_OF_EACH_STREAM = true;
	public static boolean PRINT_CONSINE_SIMILARITY_WITH_TOCKENIZED_WORD_OF_EACH_STREAM = true;//Print results tockenized words
	public static boolean PRINT_RESULTS = true;
	
	//Criteria
	public static Double COSINES_SIMILARITY_RESULT_CRITERIA = 0.0;
	
	public static int BITLY_STREAM_HIT_DELAY_SECONDS = 30; 
	
	//Twitter Filter
	public FilterQuery prepareTwitterFilter(){
		
		double[][] pakistanLcationBox = TwitterConstants.getPakistanLocationBoundryBox();
		String[] languages = new String[]{"en"};
		return new FilterQuery().locations(pakistanLcationBox).language(languages);
	}
	
	//Bitly FIlter
	public FilterPhrases preparetBitlyFilterPhrasesCriteria(){
		
		FilterPhrases filterPhrases = new FilterPhrases();
		filterPhrases.setQuery("%20");
		filterPhrases.setCities("pk");
		filterPhrases.setLang("en");
		filterPhrases.setDebug(DEBUG_BITLY_STREAM);
		
		return filterPhrases;
	}
	
}
