package utility;

public class IConstants {
	
	public static String stopWordsFilePath = "src/stopwords.txt";
	public static int TITLE_TIME_WINDOW_IN_MINUTES = 1;
	
	//Data Stream Constant
	public static int TWITTER_DATA_STREAM = 1;//Twitter
	public static int BITLY_DATA_STREAM = 2;//Bitly
	public static int TWITTER_BITLY_DATA_STREAM = 3;//Both Twitter and Bitly
	
	

}
