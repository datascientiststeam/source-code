/*
 * Copyright 2012 Sentric. All rights reserved.
 */
package twitter.spouts;

import backtype.storm.Config;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import utility.Configuration;
import utility.TwitterConstants;

public class TwitterFilterSpout extends BaseRichSpout {
	private static final long serialVersionUID = -2871793574597747583L;
	SpoutOutputCollector _collector;
    LinkedBlockingQueue<Status> queue = null;
    TwitterStream _twitterStream;
    String _username;
    String _pwd;
    boolean debug;
    
    
    public TwitterFilterSpout(String username, String pwd, boolean debug) {
        _username = username;
        _pwd = pwd;
        this.debug = debug;
    }
    
    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        queue = new LinkedBlockingQueue<Status>(1000);
        _collector = collector;
        StatusListener listener = new StatusListener() {

            @Override
            public void onStatus(Status status) {
//            	System.out.println(status.getGeoLocation());
            	if(debug){
            		System.out.println("Tweet : "+status.getText());
	            	//System.out.println("Langurage : "+status.getLang());
	            	//System.out.println("Location: "+status.getPlace());
            	}
                queue.offer(status);
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice sdn) {
            }

            @Override
            public void onTrackLimitationNotice(int i) {
            }

            @Override
            public void onScrubGeo(long l, long l1) {
            }

            @Override
            public void onException(Exception e) {
            }

			@Override
			public void onStallWarning(StallWarning arg0) {
				// TODO Auto-generated method stub
				
			}
            
        };
        
//        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder().setUser(_username).setPassword(_pwd);
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setDebugEnabled(true)
          .setOAuthConsumerKey("4wLH6JDTxLsWIfIju9CyOdy34")
          .setOAuthConsumerSecret("fx1EDOSVzjOvaMeARpNF4dC8foNDCuWBgyz1lpwkgFoxqeQEwA")
          .setOAuthAccessToken("548443812-CtGmilyt78DMBc4btgry0M5JYMzCf5SaPrzOGEDv")
          .setOAuthAccessTokenSecret("8KK1Z2mayDEvviT9ytaVc3zHZ6jwozaokMetSm638rvnL");
        
        TwitterStreamFactory fact = new TwitterStreamFactory(configurationBuilder.build());
        _twitterStream = fact.getInstance();
        _twitterStream.addListener(listener);
        //_twitterStream.sample();
        _twitterStream.filter(new Configuration().prepareTwitterFilter());
        
       
    }

    @Override
    public void nextTuple() {
        Status ret = queue.poll();
        if(ret==null) {
            Utils.sleep(50);
        } else {
            _collector.emit(new Values(ret));
        }
    }

    @Override
    public void close() {
        _twitterStream.shutdown();
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config ret = new Config();
        ret.setMaxTaskParallelism(1);
        return ret;
    }    

    @Override
    public void ack(Object id) {
    }

    @Override
    public void fail(Object id) {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweet"));
    }
    
}
