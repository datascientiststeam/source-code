/*
 * Copyright 2012 Sentric. All rights reserved.
 */
package twitter.spouts;

import backtype.storm.Config;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import twitter4j.DirectMessage;
import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.User;
import twitter4j.UserList;
import twitter4j.UserStreamListener;
import utility.Configuration;
import utility.TwitterConstants;

public class TwitterUserSpout extends BaseRichSpout {
	private static final long serialVersionUID = -2871793574597747583L;
	SpoutOutputCollector _collector;
    LinkedBlockingQueue<Status> queue = null;
    TwitterStream _twitterStream;
    String _username;
    String _pwd;
    boolean debug;
    
    
    public TwitterUserSpout(String username, String pwd, boolean debug) {
        _username = username;
        _pwd = pwd;
        this.debug = debug;
    }
    
    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        queue = new LinkedBlockingQueue<Status>(1000);
        _collector = collector;
        UserStreamListener listener = new UserStreamListener() {

            @Override
            public void onStatus(Status status) {
//            	System.out.println(status.getGeoLocation());
            	if(debug){
            		System.out.println("Tweet : "+status.getText());
	            	//System.out.println("Langurage : "+status.getLang());
	            	//System.out.println("Location: "+status.getPlace());
            	}
                queue.offer(status);
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                //System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
            }

            @Override
            public void onDeletionNotice(long directMessageId, long userId) {
                //System.out.println("Got a direct message deletion notice id:" + directMessageId);
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                //System.out.println("Got a track limitation notice:" + numberOfLimitedStatuses);
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                //System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
            }

            @Override
            public void onStallWarning(StallWarning warning) {
                //System.out.println("Got stall warning:" + warning);
            }

            @Override
            public void onFriendList(long[] friendIds) {
                //System.out.print("onFriendList");
                //for (long friendId : friendIds) {
                    //System.out.print(" " + friendId);
                //}
                //System.out.println();
            }

            @Override
            public void onFavorite(User source, User target, Status favoritedStatus) {
                //System.out.println("onFavorite source:@"
                        //+ source.getScreenName() + " target:@"
                        //+ target.getScreenName() + " @"
                        //+ favoritedStatus.getUser().getScreenName() + " - "
                        //+ favoritedStatus.getText());
            }

            @Override
            public void onUnfavorite(User source, User target, Status unfavoritedStatus) {
                //System.out.println("onUnFavorite source:@"
                        //+ source.getScreenName() + " target:@"
                        //+ target.getScreenName() + " @"
                        //+ unfavoritedStatus.getUser().getScreenName()
                        //+ " - " + unfavoritedStatus.getText());
            }

            @Override
            public void onFollow(User source, User followedUser) {
                //System.out.println("onFollow source:@"
                       // + source.getScreenName() + " target:@"
                       // + followedUser.getScreenName());
            }

            @Override
            public void onUnfollow(User source, User followedUser) {
                //System.out.println("onFollow source:@"
                       // + source.getScreenName() + " target:@"
                      //  + followedUser.getScreenName());
            }

            @Override
            public void onDirectMessage(DirectMessage directMessage) {
                //System.out.println("onDirectMessage text:"
                      //  + directMessage.getText());
            }

            @Override
            public void onUserListMemberAddition(User addedMember, User listOwner, UserList list) {
                //System.out.println("onUserListMemberAddition added member:@"
                     //   + addedMember.getScreenName()
                      //  + " listOwner:@" + listOwner.getScreenName()
                    //    + " list:" + list.getName());
            }

            @Override
            public void onUserListMemberDeletion(User deletedMember, User listOwner, UserList list) {
                //System.out.println("onUserListMemberDeleted deleted member:@"
                    //    + deletedMember.getScreenName()
                     //   + " listOwner:@" + listOwner.getScreenName()
                     //   + " list:" + list.getName());
            }

            @Override
            public void onUserListSubscription(User subscriber, User listOwner, UserList list) {
                //System.out.println("onUserListSubscribed subscriber:@"
                    //    + subscriber.getScreenName()
                     //   + " listOwner:@" + listOwner.getScreenName()
                    //    + " list:" + list.getName());
            }

            @Override
            public void onUserListUnsubscription(User subscriber, User listOwner, UserList list) {
                //System.out.println("onUserListUnsubscribed subscriber:@"
                      //  + subscriber.getScreenName()
                     //   + " listOwner:@" + listOwner.getScreenName()
                    //    + " list:" + list.getName());
            }

            @Override
            public void onUserListCreation(User listOwner, UserList list) {
                //System.out.println("onUserListCreated  listOwner:@"
                    //    + listOwner.getScreenName()
                    //    + " list:" + list.getName());
            }

            @Override
            public void onUserListUpdate(User listOwner, UserList list) {
                //System.out.println("onUserListUpdated  listOwner:@"
                    //    + listOwner.getScreenName()
                     //   + " list:" + list.getName());
            }

            @Override
            public void onUserListDeletion(User listOwner, UserList list) {
                //System.out.println("onUserListDestroyed  listOwner:@"
                    //    + listOwner.getScreenName()
                    //    + " list:" + list.getName());
            }

            @Override
            public void onUserProfileUpdate(User updatedUser) {
                //System.out.println("onUserProfileUpdated user:@" + updatedUser.getScreenName());
            }

            @Override
            public void onBlock(User source, User blockedUser) {
                //System.out.println("onBlock source:@" + source.getScreenName()
                       // + " target:@" + blockedUser.getScreenName());
            }

            @Override
            public void onUnblock(User source, User unblockedUser) {
              //  System.out.println("onUnblock source:@" + source.getScreenName()
                       // + " target:@" + unblockedUser.getScreenName());
            }

            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
                System.out.println("onException:" + ex.getMessage());
            }
            
        };
        
//        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder().setUser(_username).setPassword(_pwd);
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setDebugEnabled(true)
          .setOAuthConsumerKey("4wLH6JDTxLsWIfIju9CyOdy34")
          .setOAuthConsumerSecret("fx1EDOSVzjOvaMeARpNF4dC8foNDCuWBgyz1lpwkgFoxqeQEwA")
          .setOAuthAccessToken("548443812-CtGmilyt78DMBc4btgry0M5JYMzCf5SaPrzOGEDv")
          .setOAuthAccessTokenSecret("8KK1Z2mayDEvviT9ytaVc3zHZ6jwozaokMetSm638rvnL");
        
        TwitterStreamFactory fact = new TwitterStreamFactory(configurationBuilder.build());
        _twitterStream = fact.getInstance();
        _twitterStream.addListener(listener);
        //_twitterStream.sample();
        //_twitterStream.filter(new Configuration().prepareTwitterFilter());
        _twitterStream.user();
        
       
    }

    @Override
    public void nextTuple() {
        Status ret = queue.poll();
        if(ret==null) {
            Utils.sleep(50);
        } else {
            _collector.emit(new Values(ret));
        }
    }

    @Override
    public void close() {
        _twitterStream.shutdown();
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config ret = new Config();
        ret.setMaxTaskParallelism(1);
        return ret;
    }    

    @Override
    public void ack(Object id) {
    }

    @Override
    public void fail(Object id) {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweet"));
    }
    
}
