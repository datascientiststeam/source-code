/*
 * Copyright 2012 Sentric. All rights reserved.
 */
package twitter.bolts;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dto.DataStream;

import twitter4j.Status;
import utility.Configuration;
import utility.GlobalTime;
import utility.IConstants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class TwitterTittleTImeWindowBolt extends BaseRichBolt {
	private static final long serialVersionUID = 4150732456907856015L;
	private OutputCollector collector;
	Pattern clientPattern = Pattern.compile("<a[^>]+>([^<]+)</a>");
	
	
	Date startDate = null;
	
	//LinkedBlockingQueue<Status> queue = null;
	HashMap<Date,LinkedBlockingQueue<Status>> queueByTime = new HashMap<Date,LinkedBlockingQueue<Status>>();

	@Override
	public void prepare(final Map stormConf, final TopologyContext context,
			final OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void execute(final Tuple input) {
		final Status status = (Status) input.getValue(0);
		final String client = status.getSource();
		
		Date currentDate = new Date();

		//Current title time window datetime
		Date currentTitleTimeWindow = GlobalTime.nextTitleTimeWindowDateTime;
		
		//Prepare tweets by title time window datetime
		LinkedBlockingQueue<Status> queue= queueByTime.get(currentTitleTimeWindow);
		if(queue==null){
			queue = new LinkedBlockingQueue<Status>();
			queueByTime.put(currentTitleTimeWindow, queue);
		}
		queue.offer(status);
		
		
		if(queueByTime!=null && queueByTime.size()>0){
			
			ArrayList<Date> removeKeys = new ArrayList<Date>();
			
			for (Entry<Date, LinkedBlockingQueue<Status>> queueTime : queueByTime.entrySet()) {
				
			    //System.out.println("Key = " + queueTime.getKey() + ", Value = " + queueTime.getValue());
				Date time = queueTime.getKey();
				LinkedBlockingQueue<Status> currentQueue = queueTime.getValue();
				
				//Check time against global clock
				if(currentDate.equals(time) || currentDate.after(time)){
					
					//Needs to remove this queue against time
					removeKeys.add(time);
					
					//Debug on then show tweets arrival time
					if(Configuration.DEBUG_TITLE_TIME_WINDOW_TIMEDATE){
						System.out.println("------------------Title Time Window Current (Twitter) :  " + time);
					}
					
					//Preapare Data Stream Object
					DataStream twiterDataStream = new DataStream();
					twiterDataStream.setDataStreamCategory(IConstants.TWITTER_DATA_STREAM);
					twiterDataStream.setTwitterDataStream(currentQueue);
					twiterDataStream.setTwitterTitleTimeWindowTime(time);
					twiterDataStream.setTitleTimeWindowTime(time);
					
					//Send data to other bolt
					collector.emit(input, new Values(twiterDataStream));
					collector.ack(input);
				
				}
			}
			
			//Remove values which are emit to other bolts
			if(removeKeys!=null && removeKeys.size()>0){
				for(Date key:removeKeys){
					queueByTime.remove(key);
				}
			}
		}
		

		
	}
	@Override
	public void declareOutputFields(final OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("twitterTitleWindowStream"));
	}
}