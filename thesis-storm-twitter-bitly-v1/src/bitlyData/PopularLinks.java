package bitlyData;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import dto.BitlyDTO.PopularLinkDTO;
import utility.BitlyKeys;
import utility.HTTPURLConnectionUtility;

public class PopularLinks {

	public ArrayList<PopularLinkDTO> getPopularLinks(boolean debug){
		
		ArrayList<PopularLinkDTO> popularLinks = new ArrayList<PopularLinkDTO>();
		
		try{
		String url = "https://api-ssl.bitly.com";
		url += "/v3/user/popular_links?access_token="+BitlyKeys.ACCESS_TOKEN;
		
		HTTPURLConnectionUtility urlUtility = new HTTPURLConnectionUtility();
		urlUtility.setUrl(url);
		urlUtility.setMethod(HTTPURLConnectionUtility.METHOD_GET);
		String response = urlUtility.getResponseData();
		
		//System.out.println(response.toString());
		if(debug)
			System.out.println("===================Start : Popular Links====================");
		
		JSONObject jObject  = new JSONObject(response);
		//If Response is positive
		if(jObject.get("status_txt")!=null && jObject.get("status_txt").toString().equals("OK")){
			//Get Data
			JSONObject data = jObject.getJSONObject("data");
			//Get Phrases
			JSONArray phrases = data.getJSONArray("popular_links");
			
			if(phrases!=null && phrases.length()>0){
				for(int i=0;i<phrases.length();i++){
					JSONObject phrase = new JSONObject(phrases.get(i).toString());
					//System.out.println(phrase.toString());
					if(debug){
						System.out.println("Link : " + getJSONAttribute(phrase,"link","").toString() + "  Clicks : " + getJSONAttribute(phrase,"clicks","").toString());
					}
					
					PopularLinkDTO popularLink = new PopularLinkDTO();
					popularLink.setLink(getJSONAttribute(phrase,"link","").toString());
					popularLink.setClicks(getJSONAttribute(phrase,"clicks","").toString());
					popularLinks.add(popularLink);
					
				}
			}
		}
		
		if(debug)
			System.out.println("===================End: Popular Links====================");
		
		}catch(Exception e){
			System.out.println(e);
		}
		
		return popularLinks;
		
	}
	
	private Object getJSONAttribute(JSONObject jsonObj, String attribute, Object defaultValue){
		try{
			return jsonObj.get(attribute);
		}catch(Exception e){
		    return 	defaultValue;
		}
	}
}
