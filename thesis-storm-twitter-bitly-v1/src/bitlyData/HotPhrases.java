package bitlyData;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.BitlyKeys;
import utility.HTTPURLConnectionUtility;

public class HotPhrases {

	public ArrayList<JSONObject> getHotPhrases(){
		
		ArrayList<JSONObject> hotPhrases = new ArrayList<JSONObject>();
		
		try{
		String url = "https://api-ssl.bitly.com";
		url += "/v3/realtime/hot_phrases?access_token="+BitlyKeys.ACCESS_TOKEN;
		
		HTTPURLConnectionUtility urlUtility = new HTTPURLConnectionUtility();
		urlUtility.setUrl(url);
		urlUtility.setMethod(HTTPURLConnectionUtility.METHOD_GET);
		String response = urlUtility.getResponseData();
		
		//System.out.println(response.toString());
		System.out.println("===================Start: Hot Phrases====================");
		JSONObject jObject  = new JSONObject(response);
		//If Response is positive
		if(jObject.get("status_txt")!=null && jObject.get("status_txt").toString().equals("OK")){
			//Get Data
			JSONObject data = jObject.getJSONObject("data");
			//Get Phrases
			JSONArray phrases = data.getJSONArray("phrases");
			
			if(phrases!=null && phrases.length()>0){
				for(int i=0;i<phrases.length();i++){
					JSONObject phrase = new JSONObject(phrases.get(i).toString());
					hotPhrases.add(phrase);
					//System.out.println(phrase.toString());
					//System.out.println(phrase.get("phrase"));
				}
			}
		}
		System.out.println("===================End: Hot Phrases====================");
		
		}catch(Exception e){
			System.out.println(e);
		}
		return hotPhrases;
		
		
		
	}
}
