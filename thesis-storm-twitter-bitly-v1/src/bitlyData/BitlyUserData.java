package bitlyData;

import java.util.ArrayList;

import dto.BitlyDTO.LinkInfoDTO;
import dto.BitlyDTO.PopularLinkDTO;




public class BitlyUserData {

	public ArrayList<LinkInfoDTO> getUserData(boolean getOnlyPakistanLinks, boolean debug){
		
		ArrayList<LinkInfoDTO> linksInfo = new ArrayList<LinkInfoDTO>();
		
		PopularLinks popularLinks = new PopularLinks();
		LinkInfo linkInfo = new LinkInfo();
		ArrayList<PopularLinkDTO> pouplarLinks = popularLinks.getPopularLinks(debug);
		if(pouplarLinks!=null && pouplarLinks.size()>0){
			for(PopularLinkDTO popularLink : pouplarLinks){
				
				LinkInfoDTO info = linkInfo.getLinkInfo(popularLink.getLink(),true, false);
				
				if(info!=null){
					
					if(getOnlyPakistanLinks && !info.isPakistanCountryInfo()){
						continue;
					}
					linksInfo.add(info);
					
					if(debug){
						System.out.println("Link : "+ popularLink.getLink());
						System.out.println("Is Pak Country Links : "+ info.isPakistanCountryInfo());
						System.out.println("Link Title : "+ info.getHtmlTitle());
						System.out.println("");
					}
				}
			}
		}
		
		return linksInfo;
	}
}
