package NLP;

public class TFIDF_Utility {
	
	/**
	 * (This calculation is based on number of documents to be compared)
	 * 
	 * @param numberOfDocuments
	 * @param numberOfDocumentTermsContains
	 * @return
	 */
	public static Double getIDF(Integer numberOfDocuments, Integer numberOfDocumentTermsContains){
		Double idf = 0.00;
		if(numberOfDocuments > 0 && numberOfDocumentTermsContains>0){
			idf = Math.log((numberOfDocuments/numberOfDocumentTermsContains)) + 1;
		}
		return idf;
	}
	
	/**
	 * This calculation is per document
	 * 
	 * @param numberOfTimesTermAppearsInDocument // Number of times a word contain in documents
	 * @param totalNumberOfTermsInDocument // total number of words in document
	 * @return
	 */
	public static Double getTF(Integer numberOfTimesTermAppearsInDocument , Integer totalNumberOfTermsInDocument){
		Double tf = 0.00;
		if(numberOfTimesTermAppearsInDocument > 0 && totalNumberOfTermsInDocument>0){
			tf =  (numberOfTimesTermAppearsInDocument.doubleValue() / totalNumberOfTermsInDocument.doubleValue());
		}
		return tf;
	}

}
