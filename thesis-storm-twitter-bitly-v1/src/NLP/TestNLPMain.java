package NLP;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

import NLP.Document;

import snowball.ext.englishStemmer;

/**
 * @author brandonskane a text processing module as follows: A tokenizer
 *         according to the following rules: Tokenize all abbreviations
 *         containing periods as strings without periods Treat the rest of the
 *         punctuation as word separators Lowercase all letters
 * 
 *         A stemmer implemented via the Porter Stemmer package:
 *         http://tartarus.org/~martin/PorterStemmer/ Implement stopword removal
 *         using the following stopword list: http://bit.ly/1bqQWaV
 * 
 *         The word list is tokenized first, stopwords are removed, and finally
 *         the words are put through the stemmer
 */
public class TestNLPMain {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		
		try{
			//https://github.com/brandonskane/Text-Processing-Tokenizer
	
//			System.out.println("Text Processing Tokenizer");
//			System.out.println("-------------------------");
//	
//			String fileName = "src/text.txt";
//			String stopWordsFileName = "src/stopwords.txt";
//	
//			String rawInput = captureText(fileName);
//			String stopWordsString = processStopWords(stopWordsFileName);
//	
//			System.out.println(rawInput);
//	
//			String[] seperatedWords = rawInput.split(" ");
//			String[] stopWords = stopWordsString.split("\n");
//	
//			Tokenizer tokenizer = new Tokenizer(seperatedWords, stopWords);
//	
//			//tokenizer.stemWords(new Stemmer()); //Custom Stemmer
//			tokenizer.stemWords(new englishStemmer()); // Snowball Stemmer
//			String tokenizedWords = tokenizer.getProcessedWords();
//	
//			System.out.println();
//			System.out.println("Text Processing Tokenizer");
//			System.out.println("-------------------------");
//			System.out.println(tokenizedWords);
			
			System.out.println("Text Processing Tokenizer");
			System.out.println("-------------------------");
	
			String fileName = "src/doc1.txt";
			String rawInput = captureText(fileName);
			System.out.println(rawInput);
			Document doc1 = new Document(rawInput,1);
			doc1.tockenizeDocument();
			
			fileName = "src/doc2.txt";
			rawInput = captureText(fileName);
			System.out.println(rawInput);
			Document doc2 = new Document(rawInput,2);
			doc2.tockenizeDocument();

//			fileName = "src/doc3.txt";
//			rawInput = captureText(fileName);
//			System.out.println(rawInput);
//			Document doc3 = new Document(rawInput,3);
//			doc3.tockenizeDocument();

			
			DocumentsTFIDF TFIDFDocs = new DocumentsTFIDF();
			TFIDFDocs.addDocument(doc1);
			TFIDFDocs.addDocument(doc2);
//			TFIDFDocs.addDocument(doc3);
			TFIDFDocs.prepareTFIDFWeights();
			//TFIDFDocs.printMaps();
			
			
			
	
	
			System.out.println();
			System.out.println("Text Processing Tokenizer");
			System.out.println("-------------------------");
			System.out.println("*************************");
			System.out.println("Cosine Similarity");
			System.out.println("*************************");
			Double cosineSimilarity = new CosineSimilarity(TFIDFDocs).similarityBetweenTwoDocuments(doc1.getDocumentId(), doc2.getDocumentId());
			System.out.println("Doc 1 : " + doc1.getRawInput());
			System.out.println("Doc 2 : " + doc2.getRawInput());
			System.out.println("Cosine Similarity : " + cosineSimilarity);
			System.out.println("");
//			Double cosineSimilarity2 = new CosineSimilarity(TFIDFDocs).similarityBetweenTwoDocuments(doc1.getDocumentId(), doc3.getDocumentId());
//			System.out.println("Doc 1 : " + doc1.getRawInput());
//			System.out.println("Doc 3 : " + doc3.getRawInput());
//			System.out.println("Cosine Similarity : " + cosineSimilarity2);
//			System.out.println("");
//			Double cosineSimilarity3 = new CosineSimilarity(TFIDFDocs).similarityBetweenTwoDocuments(doc2.getDocumentId(), doc3.getDocumentId());
//			System.out.println("Doc 2 : " + doc2.getRawInput());
//			System.out.println("Doc 3 : " + doc3.getRawInput());
//			System.out.println("Cosine Similarity : " + cosineSimilarity3);
//			System.out.println("");
			
			
			//System.out.println(tokenizedWords);
		}catch(Exception e){
			System.out.println(e);
		}

	}

	static String captureText(String fileName) throws IOException {
		String textToProcess;
		FileInputStream inputStream = new FileInputStream(fileName);
		try {
			textToProcess = IOUtils.toString(inputStream);
		} finally {
			inputStream.close();
		}
		return textToProcess;
	}

	static String processStopWords(String fileName) throws IOException {
		String stopWords;
		FileInputStream inputStream = new FileInputStream(fileName);
		try {
			stopWords = IOUtils.toString(inputStream);
		} finally {
			inputStream.close();
		}
		return stopWords;

	}
}
