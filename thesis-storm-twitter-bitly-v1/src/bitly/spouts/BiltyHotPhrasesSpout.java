/*
 * Copyright 2012 Sentric. All rights reserved.
 */
package bitly.spouts;

import backtype.storm.Config;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import bitlyData.BurstingPhrases;
import bitlyData.HotPhrases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class BiltyHotPhrasesSpout extends BaseRichSpout {
	private static final long serialVersionUID = -2871793574597747583L;
	SpoutOutputCollector _collector;
    LinkedBlockingQueue<JSONObject> queue = null;//List of phrases
    TwitterStream _twitterStream;
    String _username;
    String _pwd;
    
    boolean stopDataStream = true;
    
    
    public BiltyHotPhrasesSpout(String username, String pwd) {
        _username = username;
        _pwd = pwd;
    }
    
    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        queue = new LinkedBlockingQueue<JSONObject>(1000);
        _collector = collector;
        
        
        try{
			//while(stopDataStream) {
				
				HotPhrases hotPhrases = new HotPhrases();
				ArrayList<JSONObject> phrases = hotPhrases.getHotPhrases();
				if(phrases!=null && phrases.size()>0){
					for(JSONObject phrase : phrases){
						queue.offer(phrase);
					}
				}
		        //queue.addAll(hotPhrases.getHotPhrases());
				
				//Thread.sleep(10*1000);
			//}
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        
    }
    
    public void addMoreDataInQueue() {
        queue = new LinkedBlockingQueue<JSONObject>();
        try{
			HotPhrases hotPhrases = new HotPhrases();
			ArrayList<JSONObject> phrases = hotPhrases.getHotPhrases();
			if(phrases!=null && phrases.size()>0){
				for(JSONObject phrase : phrases){
					queue.offer(phrase);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    @Override
    public void nextTuple() {
    	JSONObject ret = queue.poll();
        if(ret==null) {
        	Utils.sleep(5*1000);
        	addMoreDataInQueue();
            
        } else {
            _collector.emit(new Values(ret));
        }
    }

    @Override
    public void close() {
    	stopDataStream = false;
        //_twitterStream.shutdown();
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config ret = new Config();
        ret.setMaxTaskParallelism(1);
        return ret;
    }    

    @Override
    public void ack(Object id) {
    }

    @Override
    public void fail(Object id) {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweet"));
    }
    
}
