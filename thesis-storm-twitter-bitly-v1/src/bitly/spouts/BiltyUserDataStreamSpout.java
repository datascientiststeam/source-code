/*
 * Copyright 2012 Sentric. All rights reserved.
 */
package bitly.spouts;

import backtype.storm.Config;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import bitlyData.BitlyUserData;
import bitlyData.BurstingPhrases;
import bitlyData.FilterPhrases;
import bitlyData.HotPhrases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import utility.Configuration;

import org.json.JSONArray;
import org.json.JSONObject;

import dto.BitlyDataDTO;
import dto.BitlyDTO.LinkInfoDTO;

public class BiltyUserDataStreamSpout extends BaseRichSpout {
	private static final long serialVersionUID = -2871793574597747583L;
	SpoutOutputCollector _collector;
    LinkedBlockingQueue<LinkInfoDTO> queue = null;//List of phrases
    TwitterStream _twitterStream;
    boolean debug;
    
    boolean stopDataStream = true;
    
    
    public BiltyUserDataStreamSpout(boolean debug) {
    	this.debug = debug;
    }
    
	private void setBiltyDataStream() {
		BitlyUserData biltyUserData = new BitlyUserData();
		ArrayList<LinkInfoDTO> biltyDataStream = biltyUserData.getUserData(false, debug);
		if(biltyDataStream!=null && biltyDataStream.size()>0){
			for(LinkInfoDTO linkInfo: biltyDataStream){
				queue.offer(linkInfo);
			}
		}
//		FilterPhrases filterPhrases = new Configuration().preparetBitlyFilterPhrasesCriteria();
//		ArrayList<BitlyDataDTO> bitlyDataList = filterPhrases.getFilterPhrases();
//		if(bitlyDataList!=null && bitlyDataList.size()>0){
//			for(BitlyDataDTO bitlyData : bitlyDataList){
//				queue.offer(bitlyData);
//			}
//		}
	}

    
    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        queue = new LinkedBlockingQueue<LinkInfoDTO>();
        _collector = collector;
        try{
        	
			setBiltyDataStream();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        
    }
    public void addMoreDataInQueue() {
        queue = new LinkedBlockingQueue<LinkInfoDTO>();
        try{
        	setBiltyDataStream();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    @Override
    public void nextTuple() {
    	LinkInfoDTO ret = queue.poll();
        if(ret==null) {
        	Utils.sleep(Configuration.BITLY_STREAM_HIT_DELAY_SECONDS*1000);
        	addMoreDataInQueue();
            
        } else {
            _collector.emit(new Values(ret));
        }
    }

    @Override
    public void close() {
    	stopDataStream = false;
        //_twitterStream.shutdown();
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config ret = new Config();
        ret.setMaxTaskParallelism(1);
        return ret;
    }    

    @Override
    public void ack(Object id) {
    }

    @Override
    public void fail(Object id) {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweet"));
    }
    
}
