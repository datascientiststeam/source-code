package common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dto.BitlyDataDTO;
import dto.DataStream;
import dto.BitlyDTO.LinkInfoDTO;
import twitter4j.Status;
import utility.Configuration;
import utility.DateStamp;
import utility.IConstants;
import NLP.CosineSimilarity;
import NLP.Document;
import NLP.DocumentManager;
import NLP.DocumentsTFIDF;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class TFIDFAndConsineSimilarityCalculationBolt extends BaseRichBolt {
	private static final long serialVersionUID = 4150732456907856015L;
	private OutputCollector collector;
	private int dataStreamBoltNumber;
	private int totalTFIDFAndCosineSimilarityBolts;
	
	
	public int getDataStreamBoltNumber() {
		return dataStreamBoltNumber;
	}

	/**
	 * This parameter is used to get task.<br/><br/>
	 * 
	 * For example(1): <br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;There are 3 bolts and the boltNUmber is 1 and dataStreamNumber is 5<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;(dataStreamNumber % number of bolts == boltNumber)<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;(5 % 3 == 1) false. <br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;This bolt will not work on dataStream whose dataStreamPacket Number remainder with total number of bolts is not equal bolt number.<br/>

	 * For example (2): <br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;There are 3 bolts and the boltNUmber is 1 and dataStreamNumber is 4<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;(dataStreamNumber % number of bolts == boltNumber)<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;(5 % 4 == 1) true. <br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;This bolt will work on that dataStream.<br/><br/>
	 * 
	 * 
	 * @param dataStreamBoltNumber
	 */
	public void setDataStreamBoltNumber(int dataStreamBoltNumber) {
		this.dataStreamBoltNumber = dataStreamBoltNumber;
	}

	public int getTotalTFIDFAndCosineSimilarityBolts() {
		return totalTFIDFAndCosineSimilarityBolts;
	}

	public void setTotalTFIDFAndCosineSimilarityBolts(
			int totalTFIDFAndCosineSimilarityBolts) {
		this.totalTFIDFAndCosineSimilarityBolts = totalTFIDFAndCosineSimilarityBolts;
	}
	//Date in string format, Data stream
	private HashMap<String,DataStream> dataStreams = new HashMap<String,DataStream>();
	
	@Override
	public void prepare(final Map stormConf, final TopologyContext context,
			final OutputCollector collector) {
		this.collector = collector;
	}
	
	
	/**
	 * This parameter is used to get task.<br/><br/>
	 * 
	 * For example(1): dataStreamBoltNumber = 1 and totalTFIDFAndCosineSimilarityBolts = 3<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;There are 3 bolts and the boltNUmber is 1 and dataStreamNumber is 5<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;(dataStreamNumber % number of bolts == boltNumber)<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;(5 % 3 == 1) false. <br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;This bolt will not work on dataStream whose dataStreamPacket Number remainder with total number of bolts is not equal bolt number.<br/>

	 * For example(2): dataStreamBoltNumber = 2 and totalTFIDFAndCosineSimilarityBolts = 3<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;There are 3 bolts and the boltNUmber is 2 and dataStreamNumber is 5<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;(dataStreamNumber % number of bolts == boltNumber)<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;(5 % 3 == 2) true. <br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;This bolt will work on that dataStream.<br/><br/>
	 * 
	 * 
	 * @param dataStreamBoltNumber
	 */
	
	public TFIDFAndConsineSimilarityCalculationBolt(int dataStreamBoltNumber,int totalTFIDFAndCosineSimilarityBolts){
		this.setDataStreamBoltNumber(dataStreamBoltNumber);
		this.setTotalTFIDFAndCosineSimilarityBolts(totalTFIDFAndCosineSimilarityBolts);
	}

	@Override
	public void execute(final Tuple input) {
		
		try{
			DataStream dataStream =  (DataStream) input.getValue(0);
			
			System.out.println("Total Bolts : " + this.getTotalTFIDFAndCosineSimilarityBolts());
			System.out.println("Bolt Number :" + this.getDataStreamBoltNumber());
			System.out.println("Data Stream Number " + dataStream.getDataStreamPacketNumber());
			System.out.println("Reamainder (DataStreaNumber % TotalBolts)" + dataStream.getDataStreamPacketNumber()%this.getTotalTFIDFAndCosineSimilarityBolts());
			
			if(dataStream!=null 
					&& dataStream.getDataStreamPacketNumber()%this.getTotalTFIDFAndCosineSimilarityBolts()==this.getDataStreamBoltNumber()){
				
				System.out.println("Bolt Number : "+ this.getDataStreamBoltNumber() + " - Suceed");
			
				LinkedBlockingQueue<Status> twitterData = dataStream.getTwitterDataStream();
				HashMap<String,LinkInfoDTO> bitlyData = dataStream.getBitlyDataStream();
				
				//HashMap
				//Key: Bitly data, Values: (Key: Twitter Document value: cosine similarity))
				HashMap<LinkInfoDTO,HashMap<Status,Double>> documentSimilarityByBitlyData = new HashMap<LinkInfoDTO,HashMap<Status,Double>>();
				
				if(twitterData!=null && twitterData.size()>0 &&
						bitlyData!=null && bitlyData.size()>0){
					
		
					for(LinkInfoDTO bitly : bitlyData.values()){
						String bitlyTitle = bitly.getHtmlTitle();
//						String lang = bitly.getLang();
//						String cities = bitly.getCities();
						
						Document bitlyDoc = new DocumentManager().createDoument(bitlyTitle);
						int bitlyDocumentId = bitlyDoc.getDocumentId();
						bitlyDoc.tockenizeDocument();//Tockenize steming and remove stop words
						
						for(Status twitter : twitterData){
							String userName = twitter.getUser().getScreenName();
							String tweet = twitter.getText();
							
							
							Document tweetDoc = new DocumentManager().createDoument(tweet);
							int tweetDocId = tweetDoc.getDocumentId();
							tweetDoc.tockenizeDocument();//Tockenize steming and remove stop words
							
							//Add document for TF-IDF calculation
							//Create TFIDF map. One bitly string with each tweets
							DocumentsTFIDF TFIDFDocs = new DocumentsTFIDF();
							TFIDFDocs.addDocument(bitlyDoc);
							TFIDFDocs.addDocument(tweetDoc);
							TFIDFDocs.prepareTFIDFWeights();//Calculate weights for each bitly title with each tweets
							
							//Calculation cosine similarity
							Double cosineSimilarity = new CosineSimilarity(TFIDFDocs).similarityBetweenTwoDocuments(bitlyDocumentId, tweetDocId);
							
							//Prepare consine similarity map and send to antoher bolt
							HashMap<Status,Double> twitterStream = documentSimilarityByBitlyData.get(bitly);
							if(twitterStream == null){
								twitterStream = new HashMap<Status,Double>();
								documentSimilarityByBitlyData.put(bitly, twitterStream);
							}
							twitterStream.put(twitter, cosineSimilarity);
							
							//Print TF-IDF Maps
							if(Configuration.PRINT_TF_IDF_MAP_EACH_CRITERIA){
								System.out.println("-------------------------- TF-IDF Each Stream ------------------------");
								System.out.println("***Twitter***");
								System.out.println(tweetDoc.getProcessedWords());
								System.out.println("***Bitly***");
								System.out.println(bitlyDoc.getProcessedWords());
								TFIDFDocs.printMaps();
							}
							
							//Print TF-IDF When consine similarity criteria meets
							if(Configuration.PRINT_TF_IDF_MAP_EACH_SUCESS_CONSINE_SIMILARITY_CRITERIA){
								if(cosineSimilarity > Configuration.COSINES_SIMILARITY_RESULT_CRITERIA){
									System.out.println("-------------------------- TF-IDF of Success Consine Similarity Criteria Meets ------------------------");
									System.out.println("***Twitter***");
									System.out.println(tweetDoc.getProcessedWords());
									System.out.println("***Bitly***");
									System.out.println(bitlyDoc.getProcessedWords());
									TFIDFDocs.printMaps();
								}
							}
							
							//Show, tockenized data if criteria consine similarity meets
							if(Configuration.PRINT_CONSINE_SIMILARITY_WITH_TOCKENIZED_WORD_OF_EACH_STREAM){
								if(cosineSimilarity > Configuration.COSINES_SIMILARITY_RESULT_CRITERIA){
									System.out.println("-------------------------- Matched Cosine similarity cretiera ------------------------");
									System.out.println("***Twitter***");
									System.out.println(tweetDoc.getProcessedWords());
									System.out.println("***Bitly***");
									System.out.println(bitlyDoc.getProcessedWords());
									System.out.println("Cosine Similarity " + cosineSimilarity);
								}
							}
						}
						
						
						
						
		
					}
					collector.emit(input, new Values(documentSimilarityByBitlyData));
					collector.ack(input);
				}
			}
		}catch(Exception e){
			System.out.println(e.getMessage() + " " + e);
		}
		
		

		
	}
	@Override
	public void declareOutputFields(final OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("client3"));
	}
}