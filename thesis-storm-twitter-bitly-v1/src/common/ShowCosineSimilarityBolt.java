package common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dto.BitlyDataDTO;
import dto.DataStream;
import dto.BitlyDTO.LinkInfoDTO;
import twitter4j.Status;
import utility.Configuration;
import utility.DateStamp;
import utility.IConstants;
import NLP.CosineSimilarity;
import NLP.Document;
import NLP.DocumentManager;
import NLP.DocumentsTFIDF;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class ShowCosineSimilarityBolt extends BaseRichBolt {
	private static final long serialVersionUID = 4150732456907856015L;
	private OutputCollector collector;
	
	//Date in string format, Data stream
	private HashMap<String,DataStream> dataStreams = new HashMap<String,DataStream>();
	
	@Override
	public void prepare(final Map stormConf, final TopologyContext context,
			final OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void execute(final Tuple input) {
		
		try{
			
			HashMap<LinkInfoDTO,HashMap<Status,Double>> resultsMap = new HashMap<LinkInfoDTO,HashMap<Status,Double>>();
			
			HashMap<LinkInfoDTO,HashMap<Status,Double>> cosineSimilarityByBitlyData = (HashMap<LinkInfoDTO, HashMap<Status, Double>>) input.getValue(0);
			if(cosineSimilarityByBitlyData!=null && cosineSimilarityByBitlyData.size()>0){
				for (Entry<LinkInfoDTO, HashMap<Status, Double>> similarity : cosineSimilarityByBitlyData.entrySet()) {
					LinkInfoDTO bitly = similarity.getKey();
					HashMap<Status, Double> cosineSimilarityByTwitter = similarity.getValue();
					for (Entry<Status, Double> twitterSimilarity : cosineSimilarityByTwitter.entrySet()) {
						Status tweet = twitterSimilarity.getKey();
						Double cosineSimilarity = twitterSimilarity.getValue();
						
						if(Configuration.PRINT_CONSINE_SIMILARITY_OF_EACH_STREAM){
							StringBuffer str = new StringBuffer();
							str.append("------------------------------------------\n");
							str.append("Tweet:  @" + tweet.getUser().getScreenName() + " - " + tweet.getText() + "\n");
							str.append("Bitly Title : @" + " - " + bitly.getHtmlTitle() + " - "+ bitly.getCountriesStr() + "\n");
							str.append("Cosine Similarity : " + cosineSimilarity + "\n");
							System.out.println(str.toString());
						}
						
						//Prepare results Map
						if(cosineSimilarity > Configuration.COSINES_SIMILARITY_RESULT_CRITERIA){
							HashMap<Status,Double> twitterStream = resultsMap.get(bitly);
							if(twitterStream == null){
								twitterStream = new HashMap<Status,Double>();
								resultsMap.put(bitly, twitterStream);
							}
							twitterStream.put(tweet, cosineSimilarity);
						}
						
					}
				}
			}
			
			StringBuffer results = new StringBuffer();
			results.append("**************************************************************************************\n");
			results.append("     Results ("+resultsMap.size()+" Streams Matched) \n");
			results.append("**************************************************************************************\n");
			if(Configuration.PRINT_RESULTS){
				if(resultsMap!=null && resultsMap.size()>0){
					for (Entry<LinkInfoDTO, HashMap<Status, Double>> similarity : resultsMap.entrySet()) {
						LinkInfoDTO bitly = similarity.getKey();
						HashMap<Status, Double> cosineSimilarityByTwitter = similarity.getValue();
						for (Entry<Status, Double> twitterSimilarity : cosineSimilarityByTwitter.entrySet()) {
							Status tweet = twitterSimilarity.getKey();
							Double cosineSimilarity = twitterSimilarity.getValue();
	
							results.append("------------------------------------------\n");
							results.append("Tweet:  @" + tweet.getUser().getScreenName() + " - " + tweet.getText() + "\n");
							results.append("Bitly Title : @" + " - " + bitly.getHtmlTitle() + " - "+ bitly.getCountriesStr() + "\n");
							results.append("Cosine Similarity : " + cosineSimilarity + "\n");
							//System.out.println(str.toString());
						}
					}
				}
			}
			System.out.println(results.toString());
			

			
			
//			collector.emit(input, new Values(documentSimilarityByBitlyData));
//			collector.ack(input);
		
		}catch(Exception e){
			System.out.println(e.getMessage() + " " + e);
		}
		
		
		
	}
	@Override
	public void declareOutputFields(final OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("client4"));
	}
}